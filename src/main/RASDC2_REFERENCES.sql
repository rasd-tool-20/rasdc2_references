create or replace package         RASDC2_REFERENCES is
/*
// +----------------------------------------------------------------------+
// | RASD - Rapid Application Service Development                         |
//   Program: RASDC2_REFERENCES generated on 28.02.24 by user RASDDEV.
// +----------------------------------------------------------------------+
// | http://rasd.sourceforge.net                                          |
// +----------------------------------------------------------------------+
// | This program is generated form RASD version 1.                       |
// +----------------------------------------------------------------------+
*/
function version return varchar2;
function this_form return varchar2;
procedure version(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
function metadata return clob;
procedure metadata;
procedure webclient(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure main(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure rest(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure rlog(v_clob clob);
procedure form_js(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure form_css(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
  function changes(p_log out varchar2) return varchar2;

procedure openLOV(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
);
end;

/
create or replace package body         RASDC2_REFERENCES is
/*
// +----------------------------------------------------------------------+
// | RASD - Rapid Application Service Development                         |
//   Program: RASDC2_REFERENCES generated on 28.02.24 by user RASDDEV.
// +----------------------------------------------------------------------+
// | http://rasd.sourceforge.net                                          |
// +----------------------------------------------------------------------+
// | This program is generated form RASD version 1.                       |
// +----------------------------------------------------------------------+
*/
  type rtab is table of rowid          index by binary_integer;
  type ntab is table of number         index by binary_integer;
  type dtab is table of date           index by binary_integer;
  type ttab is table of timestamp      index by binary_integer;
  type ctab is table of varchar2(4000) index by binary_integer;
  type cctab is table of clob index by binary_integer;
  type itab is table of pls_integer    index by binary_integer;
  type set_type is record
  (
    visible boolean default true,
    readonly boolean default false,
    disabled boolean default false,
    required boolean default false,
    error varchar2(4000) ,
    info varchar2(4000) ,
    custom   varchar2(2000)
  );
  type stab is table of set_type index by binary_integer;
  log__ clob := '';
  set_session_block__ clob := '';
  RESTREQUEST clob := '';
  TYPE LOVrec__ IS RECORD (label varchar2(4000),id varchar2(4000) );
  TYPE LOVtab__ IS TABLE OF LOVrec__ INDEX BY BINARY_INTEGER;
  LOV__ LOVtab__;
  RESTRESTYPE varchar2(4000);  RECNUMB10                     number := 1
;  RECNUMB20                     number := 1
;  RECNUMP                       number := 1
;
  ACTION                        varchar2(4000);  GBUTTONCLR                    varchar2(4000) := 'GBUTTONCLR'
;  PAGE                          number := 0
;
  LANG                          varchar2(4000);
  PFORMID                       varchar2(4000);
  PFORM                         varchar2(4000);  GBUTTONSRC                    varchar2(4000) := RASDI_TRNSLT.text('Search',LANG)
;  GBUTTONBCK                    varchar2(4000) := '<<'
;  GBUTTONFWD                    varchar2(4000) := '>>'
;  GBUTTONSAVE                   varchar2(4000) := RASDI_TRNSLT.text('Save',LANG)
;
  GBUTTONSAVE#SET                set_type;  GBUTTONCOMPILE                varchar2(4000) := RASDI_TRNSLT.text('Compile',LANG)
;
  GBUTTONCOMPILE#SET             set_type;  GBUTTONRES                    varchar2(4000) := RASDI_TRNSLT.text('Reset',LANG)
;
  GBUTTONRES#SET                 set_type;  GBUTTONPREV                   varchar2(4000) := RASDI_TRNSLT.text('Preview',LANG)
;
  GBUTTONPREV#SET                set_type;
  ERROR                         varchar2(4000);
  MESSAGE                       varchar2(4000);
  WARNING                       varchar2(4000);
  VUSER                         varchar2(4000);
  VLOB                          varchar2(4000);
  COMPID                        number;
  COMPILEALL                    varchar2(4000);
  HINTCONTENT                   varchar2(4000);
  UNLINKDELETE                  varchar2(4000);
  PRFORMID                      ctab;
  PRFORMID#SET                   stab;
  B10FORM                       ctab;
  B10LABEL                      ctab;
  B20FORM                       ctab;
  B20LABEL                      ctab;
  PSESSSTORAGEENABLED           ctab;
  function changes(p_log out varchar2) return varchar2 is

  begin



    p_log := '/* Change LOG:

20230330 - Created new 2 version

20160627 - Included reference form future.

*/';

    return version;

 end;
function showLabel(plabel varchar2, pcolor varchar2 default 'U', pshowdialog number default 0)

return varchar2 is

   v__ varchar2(32000);

begin



v__ := RASDI_TRNSLT.text(plabel, lang);



if pshowdialog = 1 then

v__ := v__ || rasdc_hints.linkDialog(replace(replace(plabel,' ',''),'.',''),lang,replace(this_form,'2','')||'_DIALOG');

end if;



if pcolor is null then



return v__;



else



return '<font color="'||pcolor||'">'||v__||'</font>';



end if;





end;
procedure post_submit_template is

begin

RASDC_LIBRARY.checkprivileges(PFORMID);

begin

      select upper(form)

        into pform

        from RASD_FORMS

       where formid = PFORMID;

exception when others then null;

end;



if ACTION = GBUTTONSAVE or ACTION = GBUTTONCOMPILE then

  if rasdc_library.allowEditing(pformid) then

     null;

  else

     ACTION := GBUTTONSRC;

	 message := message || RASDI_TRNSLT.text('User has no privileges to save data!', lang);

  end if;

end if;





if rasdc_library.allowEditing(pformid) then

   GBUTTONSAVE#SET.visible := true;

   GBUTTONCOMPILE#SET.visible := true;

else

   GBUTTONSAVE#SET.visible := false;

   GBUTTONCOMPILE#SET.visible := false;

end if;





VUSER := rasdi_client.secGetUsername;

VLOB := rasdi_client.secGetLOB;

if lang is null then lang := rasdi_client.c_defaultLanguage; end if;



end;
procedure compile(pformid number, pform varchar2, lang varchar2,  sporocilo in out varchar2, refform varchar2 default ''  , pcompid in out number) is

      v_server RASD_ENGINES.server%type;

      cid      pls_integer;

      n        pls_integer;

      vup      varchar2(30) := rasdi_client.secGetUsername;

begin

        rasdc_library.log(this_form,pformid, 'COMPILE_S', pcompid);



        begin

          if instr( upper(rasd_client.secGet_PATH_INFO), upper(pform)) > 0

			 then

            sporocilo := RASDI_TRNSLT.text('From is not generated.', lang);

          else



            select server

              into v_server

              from RASD_FORMS_COMPILED fg, RASD_ENGINES g

             where fg.engineid = g.engineid

               and fg.formid = PFORMID

               and fg.editor = vup

               and (fg.lobid = rasdi_client.secGetLOB or

                   fg.lobid is null and rasdi_client.secGetLOB is null);



            cid     := dbms_sql.open_cursor;



            dbms_sql.parse(cid,

                           'begin ' || v_server || '.c_debug := false;'|| v_server || '.form(' || PFORMID ||

                           ',''' || lang || ''');end;',

                           dbms_sql.native);



            n       := dbms_sql.execute(cid);



            dbms_sql.close_cursor(cid);



            sporocilo := RASDI_TRNSLT.text('From is generated.', lang) || rasdc_library.checknumberofsubfields(PFORMID);



        if refform is not null then

           sporocilo :=  sporocilo || '<br/> - '||  RASDI_TRNSLT.text('To unlink referenced code check:', lang)||'<input type="checkbox" name="UNLINK" value="Y"/>.';

        end if;



          end if;

        exception

          when others then

            if sqlcode = -24344 then



            sporocilo := RASDI_TRNSLT.text('Form is generated with compilation error. Check your code.', lang)||'('||sqlerrm||')';



            else

            sporocilo := RASDI_TRNSLT.text('Form is NOT generated - internal RASD error.', lang) || '('||sqlerrm||')<br>'||

                         RASDI_TRNSLT.text('To debug run: ', lang) || 'begin ' || v_server || '.form(' || PFORMID ||

                         ',''' || lang || ''');end;' ;

            end if;

        end;

        rasdc_library.log(this_form,pformid, 'COMPILE_E', pcompid);

end;
  function poutputrest return clob;
     procedure htpClob(v_clob clob) is
        i number := 0;
        v clob := v_clob;
       begin
       while length(v) > 0 and i < 100000 loop
        htp.prn(substr(v,1,10000));
        i := i + 1;
        v := substr(v,10001);
       end loop;
       end;
     procedure rlog(v_clob clob) is
       begin
        log__ := log__ ||systimestamp||':'||v_clob||'<br/>';
        rasd_client.callLog('RASDC2_REFERENCES',v_clob, systimestamp, '' );
       end;
procedure pLog is begin htpClob('<div class="debug">'||log__||'</div>'); end;
     function FORM_UIHEAD return clob is
       begin
        return  '
<script type="text/javascript" src="RASDC2_FIELDSONBLOCK.codemirrorjs?n=B10SQLTEXT_X_RASD&h=150&d='|| PSESSSTORAGEENABLED(1) ||'"></script>
';
       end;
     function form_js return clob is
          v_out clob;
       begin
        v_out :=  '
$(function() {



//  addSpinner();

//   initRowStatus();

//   transformVerticalTable("B15_TABLE", 4 );

   setShowHideDiv("B30_DIV", true);

//   CheckFieldValue(pid , pname)

//   CheckFieldMandatory(pid , pname)





HighLightRow("referenceBlock", "#aaccf7");





$(".rasdTxB20FORMAT INPUT").attr("maxlength", 30);

$(".rasdTxB20FIELDID INPUT").attr("maxlength", 30);

$(".rasdTxB20ORDERBY INPUT").attr("maxlength", 10);

$(".rasdTxB20DEFAULTVAL INPUT").attr("maxlength", 30);

$(".rasdTxB20NAMEID INPUT").attr("maxlength", 30);

$(".rasdTxB20LABEL INPUT").attr("maxlength", 30);





    var newDT1 = JSON.parse(JSON.stringify(_rasd_dataTableProperties));

    newDT1.responsive=false;

    newDT1.bSort=false;



    newDT1.destroy = true;

    $("#B20_TABLE").removeClass( "rasdTableN" );

    xTable1 = $("#B20_TABLE").DataTable(newDT1);



    $(''a.toggle-ser'').on(''click'', function (e) {

        e.preventDefault();

        $("#SHOWHIDESER_RASD").val(!xTable1.column(1).visible());

        // Toggle the visibility

        xTable1.column(1).visible(!xTable1.column(1).visible());

        xTable1.column(2).visible(!xTable1.column(2).visible());

        xTable1.column(3).visible(!xTable1.column(3).visible());

        xTable1.column(4).visible(!xTable1.column(4).visible());

        xTable1.column(5).visible(!xTable1.column(5).visible());

        xTable1.column(6).visible(!xTable1.column(6).visible());

        xTable1.column(7).visible(!xTable1.column(7).visible());

        xTable1.column(8).visible(!xTable1.column(8).visible());

        xTable1.column(9).visible(!xTable1.column(9).visible());

        xTable1.column(10).visible(!xTable1.column(10).visible());

        xTable1.column(11).visible(!xTable1.column(11).visible());

        xTable1.column(12).visible(!xTable1.column(12).visible());





	    if ($("#SHOWHIDESER_RASD").val()==''false'') {

	       $(''.rasdTxB20NAMEID input'').css({''width'': ''200px''});

	       $(''.rasdTxB20LABEL input'').css({''width'': ''200px''});

	       $(''.rasdTxB20ELEMENT select'').css({''width'': ''200px''});

	       $(''.rasdTxB20LINKID select'').css({''width'': ''200px''});

	       $(''.rasdTxB20FIELDID input'').css({''width'': ''200px''});

		} else {

	       $(''.rasdTxB20NAMEID input'').css({''width'': ''80px''});

	       $(''.rasdTxB20LABEL input'').css({''width'': ''80px''});

	       $(''.rasdTxB20ELEMENT select'').css({''width'': ''100px''});

	       $(''.rasdTxB20LINKID select'').css({''width'': ''100px''});

		   $(''.rasdTxB20FIELDID input'').css({''width'': ''80px''});

		}



    });



      $(''a.toggle-ui'').on(''click'', function (e) {

        e.preventDefault();

        $("#SHOWHIDEUI_RASD").val(!xTable1.column(13).visible());

        // Toggle the visibility

        xTable1.column(13).visible(!xTable1.column(13).visible());

        xTable1.column(14).visible(!xTable1.column(14).visible());

        xTable1.column(15).visible(!xTable1.column(15).visible());

        xTable1.column(16).visible(!xTable1.column(16).visible());

        xTable1.column(17).visible(!xTable1.column(17).visible());

        xTable1.column(18).visible(!xTable1.column(18).visible());

        xTable1.column(19).visible(!xTable1.column(19).visible());

        xTable1.column(20).visible(!xTable1.column(20).visible());

        xTable1.column(21).visible(!xTable1.column(21).visible());



	    if ($("#SHOWHIDEUI_RASD").val()==''false'') {

	       $(''.rasdTxB20ORDERBY input'').css({''width'': ''150px''});

	       $(''.rasdTxB20FORMAT input'').css({''width'': ''200px''});

	       $(''.rasdTxB20DEFAULTVAL input'').css({''width'': ''300px''});

		   $(''.rasdTxB20FIELDID input'').css({''width'': ''200px''});

		} else {

	       $(''.rasdTxB20ORDERBY input'').css({''width'': ''80px''});

	       $(''.rasdTxB20FORMAT input'').css({''width'': ''80px''});

	       $(''.rasdTxB20DEFAULTVAL input'').css({''width'': ''80px''});

		   $(''.rasdTxB20FIELDID input'').css({''width'': ''80px''});

		}

    });



  if ($("#SHOWHIDESER_RASD").val() == ''false'') {

        xTable1.column(1).visible(false);

        xTable1.column(2).visible(false);

        xTable1.column(3).visible(false);

        xTable1.column(4).visible(false);

        xTable1.column(5).visible(false);

        xTable1.column(6).visible(false);

        xTable1.column(7).visible(false);

        xTable1.column(8).visible(false);

        xTable1.column(9).visible(false);

        xTable1.column(10).visible(false);

        xTable1.column(11).visible(false);

        xTable1.column(12).visible(false);

  }



  if ($("#SHOWHIDEUI_RASD").val() == ''false'') {

        xTable1.column(13).visible(false);

        xTable1.column(14).visible(false);

        xTable1.column(15).visible(false);

        xTable1.column(16).visible(false);

        xTable1.column(17).visible(false);

        xTable1.column(18).visible(false);

        xTable1.column(19).visible(false);

        xTable1.column(20).visible(false);

        xTable1.column(21).visible(false);

  }





// aded icon wher LOV is

ShowLOVIcon();



 });







$(function() {





  rasd_codemirror_B10SQLTEXT_X_RASD();





});


$(function() {



  addSpinner();



});



$(function() {



  $(".rasdFormMenu").html("'|| RASDC_LIBRARY.showMeni(THIS_FORM, PFORMID, null, lang) ||'");





  $(document).ready(function () {

   $(".dialog").dialog({ autoOpen: false });

  });





});
        ';
        return v_out;
       end;
     function form_css return clob is
          v_out clob;
       begin
        v_out :=  '
.rasdTxB10SQLTABLE INPUT{

   width: 300px;

}



.rasdTxPF INPUT{

   width: 300px;

}



.rasdTxB10LABEL INPUT{

   width: 300px;

}



#rasdTxB10VERSION_1 {

   text-align: center;

}



.rasdTxB20ORDERBY {

width: 50px;

}





table.dataTable tbody th, table.dataTable tbody td {

   padding: 2px 2px;

}



.rasdTextC {

    width: 80;

}



.rasdTxB20ELEMENT SELECT {

    width: 100;

}



.rasdTxB20LINKID SELECT {

    width: 100;

}



#rasdTxLabB10FORM {

    width: 400px;

}



#rasdTxLabB20FORM {

    width: 400px;

}



#RASDC2_REFERENCES_MENU ul li:nth-child(1), #RASDC2_REFERENCES_MENU ul li:nth-child(n+3){

 display: none;

}


        ';
        return v_out;
       end;
procedure form_js(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is begin htpClob(form_js); end;
procedure form_css(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is begin htpClob(form_css); end;

 procedure on_submit(name_array  in owa.vc_arr, value_array in owa.vc_arr);
 procedure on_session;
 procedure formgen_js;

function openLOV(
  p_lov varchar2,
  p_value varchar2
) return lovtab__ is
  name_array   owa.vc_arr;
  value_array  owa.vc_arr;
begin
  name_array(1) := 'PLOV';
  value_array(1) := p_lov;
  name_array(2) := 'PID';
  value_array(2) := p_value;
  name_array(3) := 'CALL';
  value_array(3) := 'PLSQL';
  openLOV(name_array, value_array);
  return lov__;
end;
procedure openLOV(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
  num_entries number := name_array.count;
cursor c_lov$RfForms(p_id varchar2) is
--<lovsql formid="93" linkid="lov$RfForms">
select id, label from (select '' id, '' label, 1 vr from dual

 union all

select distinct to_char(f.formid) id , f.form||' ('||c2.application||')' label, 2

from rasd_forms f, rasd_forms_compiled c1,  rasd_forms_compiled c2

where c1.formid = pformid

  and c1.lobid = c2.lobid

  and c1.engineid = c2.engineid

--  and c2.compileyn = 'Y'

  and c2.formid = f.formid

  and f.formid <> pformid

order by 3, 2 ) where upper(id) like '%'||upper(p_id)||'%' or upper(label) like '%'||upper(p_id)||'%'
--</lovsql>
;
TYPE pLOVType IS RECORD (
output varchar2(500),
p1 varchar2(200)
);
  TYPE tab_pLOVType IS TABLE OF pLOVType INDEX BY BINARY_INTEGER;
  v_lov tab_pLOVType;
  v_lovf tab_pLOVType;
  v_counter number := 1;
  v_description varchar2(100);
  p_lov varchar2(100);
  p_nameid varchar2(100);
  p_id varchar2(100);
  v_output boolean;
  v_call varchar2(10);
  v_hidden_fields varchar2(32000) := '';
  v_opener_tekst  varchar2(32000) := '';
  RESTRESTYPE varchar2(10);
begin
  on_submit(name_array, value_array);
  for i in 1..num_entries loop
    if name_array(i) = 'PLOV' then p_lov := value_array(i);
    elsif name_array(i) = 'FIN' then p_nameid := value_array(i);
    elsif name_array(i) = 'PID' then p_id := value_array(i);
    elsif upper(name_array(i)) = 'CALL' then v_call := value_array(i);
    elsif upper(name_array(i)) = upper('RESTRESTYPE') then RESTRESTYPE := value_array(i);
    else
      if name_array(i) not in ('LOVlist') then
        v_hidden_fields := v_hidden_fields||'<input type="hidden" name="'||name_array(i)||'" value="'||value_array(i)||'" />';
      end if;
    end if;
  end loop;
    if v_call not in ('PLSQL','REST') then
      on_session;
    end if;
  if lower(p_lov) = lower('lov$RfForms') then
    v_description := 'REFERENCED FORMS';
    for r in c_lov$RfForms(p_id) loop
        v_lov(v_counter).p1 := r.id;
        v_lov(v_counter).output := r.label;
        v_counter := v_counter + 1;
    end loop;
    v_counter := v_counter - 1;
        if 1=2 then null;
        end if;
  else
   return;
  end if;
if v_call = 'PLSQL' then
  lov__.delete;
  for i in 1..v_lov.count loop
   lov__(i).id := v_lov(i).p1;
   lov__(i).label := v_lov(i).output;
  end loop;
elsif v_call = 'REST' then
if RESTRESTYPE = 'XML' then
    OWA_UTIL.mime_header('text/xml', FALSE);
    OWA_UTIL.http_header_close;
 htp.p('<?xml version="1.0" encoding="UTF-8"?>
<openLOV LOV="'||p_lov||'" filter="'||p_id||'">');
 htp.p('<result>');
 for i in 1..v_counter loop
 htp.p('<element><code>'||v_lov(i).p1||'</code><description>'||v_lov(i).output||'</description></element>');
 end loop;
 htp.p('</result></openLOV>');
else
    OWA_UTIL.mime_header('application/json', FALSE);
    OWA_UTIL.http_header_close;
 htp.p('{"openLOV":{"@LOV":"'||p_lov||'","@filter":"'||p_id||'",' );
 htp.p('"result":[');
 for i in 1..v_counter loop
  if i = 1 then
 htp.p('{"code":"'||v_lov(i).p1||'","description":"'||v_lov(i).output||'"}');
  else
 htp.p(',{"code":"'||v_lov(i).p1||'","description":"'||v_lov(i).output||'"}');
  end if;
 end loop;
 htp.p(']}}');
end if;
else
 htp.p('
<html>');
    htp.prn('<head>
<head>');
htpClob(rasd_client.getHtmlJSLibrary('HEAD',''|| rasdc_library.formName(PFORMID, LANG) ||''));
htp.p('');
htp.p('<script type="text/javascript">');
formgen_js;
htp.p('</script>');
htpClob(FORM_UIHEAD);
htp.p('<style type="text/css">');
htpClob(FORM_CSS);
htp.p('</style><script type="text/javascript">');  htp.p('</script>');

htp.prn('</head></head>
    ');
 htp.bodyOpen('','');
htp.p('
<script language="JavaScript">
        $(function() {
        document.getElementById("PID").select();
        });
   function closeLOV() {
     this.close();
   }
   function selectLOV() {
     var value = window.document.'||p_lov||'.LOVlist.options[window.document.'||p_lov||'.LOVlist.selectedIndex].value;
     var tekst = window.document.'||p_lov||'.LOVlist.options[window.document.'||p_lov||'.LOVlist.selectedIndex].text;
     window.opener.'||p_nameid||'.value = value;
     '||v_opener_tekst||'
     event = new Event(''change'');
     window.opener.'||p_nameid||'.dispatchEvent(event);
     ');
htp.p('this.close ();
   }
  with (document) {
  if (screen.availWidth < 900){
    moveTo(-4,-4)}
  }
</script>');
 htp.p('<div class="rasdLovName">'||v_description||'</div>');
 htp.formOpen(curl=>'!RASDC2_REFERENCES.openLOV',
                 cattributes=>'name="'||p_lov||'"');
 htp.p('<input type="hidden" name="PLOV" value="'||p_lov||'">');
 htp.p('<input type="hidden" name="FIN" value="'||p_nameid||'">');
 htp.p(v_hidden_fields);
 htp.p('<div class="rasdLov" align="center"><center>');
 htp.p('Filter:<input type="text" id="PID" autofocus="autofocus" name="PID" value="'||p_id||'" ></BR><input type="submit" class="rasdButton" value="Search"><input class="rasdButton" type="button" value="Clear" onclick="document.'||p_lov||'.PID.value=''''; document.'||p_lov||'.submit();"></BR>');
 htp.formselectOpen('LOVlist',cattributes=>'size=15 width="100%"');
 for i in 1..v_counter loop
  if i = 1 then -- fokus na prvem
    htp.formSelectOption(cvalue=>v_lov(i).output,cselected=>1,Cattributes => 'value="'||v_lov(i).p1||'"');
  else
    htp.formSelectOption(cvalue=>v_lov(i).output,Cattributes => 'value="'||v_lov(i).p1||'"');
  end if;
 end loop;
 htp.formselectClose;
 htp.p('');
 htp.line;
 htp.p('<input type="button" class="rasdButton" value="Select and Confirm" onClick="selectLOV();">');
 htp.p('<input type="button" class="rasdButton" value="Close" onClick="closeLOV();">');
 htp.p('</center></div>');
 htp.p('</form>');
 htp.p('</body>');
 htp.p('</html>');
end if;
end;
  function version return varchar2 is
  begin
   return 'v.1.1.20240228091044';
  end;
  function this_form return varchar2 is
  begin
   return 'RASDC2_REFERENCES';
  end;
procedure version(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is
  begin
   htp.p( version );
  end;
  procedure on_session is
    i__ pls_integer := 1;
  begin
  if ACTION is not null then
set_session_block__ := set_session_block__ || 'begin ';
set_session_block__ := set_session_block__ || 'rasd_client.sessionStart;';
set_session_block__ := set_session_block__ || ' rasd_client.sessionClose;';
set_session_block__ := set_session_block__ || 'exception when others then null; rasd_client.sessionClose; end;';
  else
declare vc varchar2(2000); begin
null;
exception when others then  null; end;
  end if;
  end;
  procedure on_readrest is
    i__ pls_integer := 1;
  begin
for r__  in (select * from json_table( RESTREQUEST , '$.form.formfields' COLUMNS(
   x__ varchar2(1) PATH '$.X__'
  ,RECNUMB10 number PATH '$.recnumb10'
  ,RECNUMB20 number PATH '$.recnumb20'
  ,RECNUMP number PATH '$.recnump'
  ,ACTION varchar2(4000) PATH '$.action'
  ,PAGE number PATH '$.page'
  ,LANG varchar2(4000) PATH '$.lang'
  ,PFORMID varchar2(4000) PATH '$.pformid'
  ,GBUTTONSAVE varchar2(4000) PATH '$.gbuttonsave'
  ,GBUTTONCOMPILE varchar2(4000) PATH '$.gbuttoncompile'
  ,GBUTTONRES varchar2(4000) PATH '$.gbuttonres'
  ,GBUTTONPREV varchar2(4000) PATH '$.gbuttonprev'
  ,HINTCONTENT varchar2(4000) PATH '$.hintcontent'
)) jt ) loop
 if instr(RESTREQUEST,'recnumb10') > 0 then RECNUMB10 := r__.RECNUMB10; end if;
 if instr(RESTREQUEST,'recnumb20') > 0 then RECNUMB20 := r__.RECNUMB20; end if;
 if instr(RESTREQUEST,'recnump') > 0 then RECNUMP := r__.RECNUMP; end if;
 if instr(RESTREQUEST,'action') > 0 then ACTION := r__.ACTION; end if;
 if instr(RESTREQUEST,'page') > 0 then PAGE := r__.PAGE; end if;
 if instr(RESTREQUEST,'lang') > 0 then LANG := r__.LANG; end if;
 if instr(RESTREQUEST,'pformid') > 0 then PFORMID := r__.PFORMID; end if;
 if instr(RESTREQUEST,'gbuttonsave') > 0 then GBUTTONSAVE := r__.GBUTTONSAVE; end if;
 if instr(RESTREQUEST,'gbuttoncompile') > 0 then GBUTTONCOMPILE := r__.GBUTTONCOMPILE; end if;
 if instr(RESTREQUEST,'gbuttonres') > 0 then GBUTTONRES := r__.GBUTTONRES; end if;
 if instr(RESTREQUEST,'gbuttonprev') > 0 then GBUTTONPREV := r__.GBUTTONPREV; end if;
 if instr(RESTREQUEST,'hintcontent') > 0 then HINTCONTENT := r__.HINTCONTENT; end if;
end loop;
i__ := 1;for r__  in (select * from json_table( RESTREQUEST , '$.form.p' COLUMNS(
   x__ varchar2(1) PATH '$.rs'
  ,PRFORMID varchar2(4000) PATH '$.prformid'
  ,PSESSSTORAGEENABLED varchar2(4000) PATH '$.sessstorageenabled'
)) jt ) loop
 if instr(RESTREQUEST,'prformid') > 0 then PRFORMID(i__) := r__.PRFORMID; end if;
 if instr(RESTREQUEST,'sessstorageenabled') > 0 then PSESSSTORAGEENABLED(i__) := r__.PSESSSTORAGEENABLED; end if;
i__ := i__ + 1;
end loop;
i__ := 1;for r__  in (select * from json_table( RESTREQUEST , '$.form.b10[*]' COLUMNS(
   x__ varchar2(1) PATH '$.rs'
)) jt ) loop
if r__.x__ is not null then
i__ := i__ + 1;
end if;
end loop;
i__ := 1;for r__  in (select * from json_table( RESTREQUEST , '$.form.b20[*]' COLUMNS(
   x__ varchar2(1) PATH '$.rs'
)) jt ) loop
if r__.x__ is not null then
i__ := i__ + 1;
end if;
end loop;
  end;
  function validate_submit(v_text varchar2) return varchar2 is
    v_outt varchar2(32000) := v_text;
  begin
    if instr(v_outt,'"') > 0 then v_outt := replace(v_outt,'"','&quot;');
    elsif instr(v_outt,'%22') > 0 then v_outt := replace(v_outt,'%22','&quot;');
    elsif instr(lower(v_outt),'<script') > 0 then v_outt := replace(v_outt,'<script','&lt;script');
    end if;
    return v_outt;
  end;
  procedure on_submit(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
    num_entries number := name_array.count;
    v_max  pls_integer := 0;
  begin
-- submit fields
    for i__ in 1..nvl(num_entries,0) loop
      if 1 = 2 then null;
      elsif  upper(name_array(i__)) = 'RESTRESTYPE' then RESTRESTYPE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = 'RESTREQUEST' then RESTREQUEST := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('RECNUMB10') then RECNUMB10 := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('RECNUMB20') then RECNUMB20 := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('RECNUMP') then RECNUMP := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('ACTION') then ACTION := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONCLR') then GBUTTONCLR := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PAGE') then PAGE := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('LANG') then LANG := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PFORMID') then PFORMID := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PFORM') then PFORM := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONSRC') then GBUTTONSRC := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONBCK') then GBUTTONBCK := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONFWD') then GBUTTONFWD := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONSAVE') then GBUTTONSAVE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONCOMPILE') then GBUTTONCOMPILE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONRES') then GBUTTONRES := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONPREV') then GBUTTONPREV := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('ERROR') then ERROR := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('MESSAGE') then MESSAGE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('WARNING') then WARNING := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('VUSER') then VUSER := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('VLOB') then VLOB := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('COMPID') then COMPID := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('COMPILEALL') then COMPILEALL := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('HINTCONTENT') then HINTCONTENT := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('UNLINKDELETE') then UNLINKDELETE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PRFORMID_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PRFORMID(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10FORM_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10FORM(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10LABEL_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10LABEL(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B20FORM_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B20FORM(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B20LABEL_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B20LABEL(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('SESSSTORAGEENABLED_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PSESSSTORAGEENABLED(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PRFORMID') and PRFORMID.count = 0 and value_array(i__) is not null then
        PRFORMID(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10FORM') and B10FORM.count = 0 and value_array(i__) is not null then
        B10FORM(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10LABEL') and B10LABEL.count = 0 and value_array(i__) is not null then
        B10LABEL(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B20FORM') and B20FORM.count = 0 and value_array(i__) is not null then
        B20FORM(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B20LABEL') and B20LABEL.count = 0 and value_array(i__) is not null then
        B20LABEL(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('SESSSTORAGEENABLED') and PSESSSTORAGEENABLED.count = 0 and value_array(i__) is not null then
        PSESSSTORAGEENABLED(1) := validate_submit(value_array(i__));
      end if;
    end loop;
-- organize records
declare
v_last number :=
PRFORMID
.last;
v_curr number :=
PRFORMID
.first;
i__ number;
begin
 if v_last <>
PRFORMID
.count then
   v_curr :=
PRFORMID
.FIRST;
   i__ := 1;
   WHILE v_curr IS NOT NULL LOOP
      if PRFORMID.exists(v_curr) then PRFORMID(i__) := PRFORMID(v_curr); end if;
      if PSESSSTORAGEENABLED.exists(v_curr) then PSESSSTORAGEENABLED(i__) := PSESSSTORAGEENABLED(v_curr); end if;
      i__ := i__ + 1;
      v_curr :=
PRFORMID
.NEXT(v_curr);
   END LOOP;
      PRFORMID.DELETE(i__ , v_last);
      PSESSSTORAGEENABLED.DELETE(i__ , v_last);
end if;
end;
declare
v_last number :=
B10FORM
.last;
v_curr number :=
B10FORM
.first;
i__ number;
begin
 if v_last <>
B10FORM
.count then
   v_curr :=
B10FORM
.FIRST;
   i__ := 1;
   WHILE v_curr IS NOT NULL LOOP
      if B10FORM.exists(v_curr) then B10FORM(i__) := B10FORM(v_curr); end if;
      if B10LABEL.exists(v_curr) then B10LABEL(i__) := B10LABEL(v_curr); end if;
      i__ := i__ + 1;
      v_curr :=
B10FORM
.NEXT(v_curr);
   END LOOP;
      B10FORM.DELETE(i__ , v_last);
      B10LABEL.DELETE(i__ , v_last);
end if;
end;
declare
v_last number :=
B20FORM
.last;
v_curr number :=
B20FORM
.first;
i__ number;
begin
 if v_last <>
B20FORM
.count then
   v_curr :=
B20FORM
.FIRST;
   i__ := 1;
   WHILE v_curr IS NOT NULL LOOP
      if B20FORM.exists(v_curr) then B20FORM(i__) := B20FORM(v_curr); end if;
      if B20LABEL.exists(v_curr) then B20LABEL(i__) := B20LABEL(v_curr); end if;
      i__ := i__ + 1;
      v_curr :=
B20FORM
.NEXT(v_curr);
   END LOOP;
      B20FORM.DELETE(i__ , v_last);
      B20LABEL.DELETE(i__ , v_last);
end if;
end;
-- init fields
    v_max := 0;
    if B10FORM.count > v_max then v_max := B10FORM.count; end if;
    if B10LABEL.count > v_max then v_max := B10LABEL.count; end if;
    if v_max = 0 then v_max := 0; end if;
    for i__ in 1..v_max loop
      if not B10FORM.exists(i__) then
        B10FORM(i__) := null;
      end if;
      if not B10LABEL.exists(i__) then
        B10LABEL(i__) := null;
      end if;
    null; end loop;
    v_max := 0;
    if B20FORM.count > v_max then v_max := B20FORM.count; end if;
    if B20LABEL.count > v_max then v_max := B20LABEL.count; end if;
    if v_max = 0 then v_max := 0; end if;
    for i__ in 1..v_max loop
      if not B20FORM.exists(i__) then
        B20FORM(i__) := null;
      end if;
      if not B20LABEL.exists(i__) then
        B20LABEL(i__) := null;
      end if;
    null; end loop;
    v_max := 0;
    if PRFORMID.count > v_max then v_max := PRFORMID.count; end if;
    if PSESSSTORAGEENABLED.count > v_max then v_max := PSESSSTORAGEENABLED.count; end if;
    if v_max = 0 then v_max := 1; end if;
    for i__ in 1..v_max loop
      if not PRFORMID.exists(i__) then
        PRFORMID(i__) := null;
      end if;
      if not PRFORMID#SET.exists(i__) then
        PRFORMID#SET(i__).visible := true;
      end if;
      if not PSESSSTORAGEENABLED.exists(i__) then
        PSESSSTORAGEENABLED(i__) := null;
      end if;
    null; end loop;
  end;
  procedure post_submit is
  begin
--<POST_SUBMIT formid="93" blockid="">
----put procedure in the begining of trigger;

post_submit_template;





GBUTTONCOMPILE := RASDI_TRNSLT.text('Multi compile',lang);
--</POST_SUBMIT>
    null;
  end;
  procedure psubmit(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
  begin
-- Reading post variables into fields.
    on_submit(name_array ,value_array); on_session;
    post_submit;
  end;
  procedure psubmitrest(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
  begin
-- Reading post variables into fields.
    on_submit(name_array ,value_array); on_readrest;
    post_submit;
  end;
  procedure pclear_P(pstart number) is
    i__ pls_integer;
    j__ pls_integer;
    k__ pls_integer;
    v_numrows pls_integer := 1;
  begin
      i__ := pstart;
      if v_numrows = 0 then k__ := i__ + 0;
 if pstart = 0 then k__ := k__ +
PRFORMID
.count(); end if;
      else
       if i__ > 1 then  k__ := i__ + 0;
       else k__ := 0 + 1;
       end if;
      end if;
      j__ := i__;
if pstart = 0 and 0 + 1=0 then j__ := 0; k__:= 1; end if;
      for i__ in j__+1..k__ loop
-- Generated initialization of the fields in new record. Use (i__) to access fields values.
        PRFORMID(i__) := null;
        PSESSSTORAGEENABLED(i__) := null;
        PRFORMID#SET(i__).visible := true;

      end loop;
  end;
  procedure pclear_B10(pstart number) is
    i__ pls_integer;
    j__ pls_integer;
    k__ pls_integer;
    v_numrows pls_integer := 0;
  begin
      i__ := pstart;
      if v_numrows = 0 then k__ := i__ + 0;
      else
       if i__ > 0 then  k__ := i__ + 0;
       else k__ := 0 + 0;
       end if;
      end if;
      j__ := i__;
if pstart = 0 then B10FORM.delete; end if;
      for i__ in 1..j__ loop
      null;
      end loop;
      for i__ in j__+1..k__ loop
-- Generated initialization of the fields in new record. Use (i__) to access fields values.
        B10FORM(i__) := null;
        B10LABEL(i__) := null;

      end loop;
  end;
  procedure pclear_B20(pstart number) is
    i__ pls_integer;
    j__ pls_integer;
    k__ pls_integer;
    v_numrows pls_integer := 0;
  begin
      i__ := pstart;
      if v_numrows = 0 then k__ := i__ + 0;
      else
       if i__ > 0 then  k__ := i__ + 0;
       else k__ := 0 + 0;
       end if;
      end if;
      j__ := i__;
if pstart = 0 then B20FORM.delete; end if;
      for i__ in 1..j__ loop
      null;
      end loop;
      for i__ in j__+1..k__ loop
-- Generated initialization of the fields in new record. Use (i__) to access fields values.
        B20FORM(i__) := null;
        B20LABEL(i__) := null;

      end loop;
  end;
  procedure pclear_form is
  begin
    RECNUMB10 := 1;
    RECNUMB20 := 1;
    RECNUMP := 1;
    GBUTTONCLR := 'GBUTTONCLR';
    PAGE := 0;
    LANG := null;
    PFORMID := null;
    PFORM := null;
    GBUTTONSRC := RASDI_TRNSLT.text('Search',LANG);
    GBUTTONBCK := '<<';
    GBUTTONFWD := '>>';
    GBUTTONSAVE := RASDI_TRNSLT.text('Save',LANG);
    GBUTTONCOMPILE := RASDI_TRNSLT.text('Compile',LANG);
    GBUTTONRES := RASDI_TRNSLT.text('Reset',LANG);
    GBUTTONPREV := RASDI_TRNSLT.text('Preview',LANG);
    ERROR := null;
    MESSAGE := null;
    WARNING := null;
    VUSER := null;
    VLOB := null;
    COMPID := null;
    COMPILEALL := null;
    HINTCONTENT := null;
    UNLINKDELETE := null;
  null; end;
  procedure pclear is
  begin
-- Clears all fields on form and blocks.
    pclear_form;
    pclear_P(0);
    pclear_B10(0);
    pclear_B20(0);

  null;
  end;
  procedure pselect_P is
    i__ pls_integer;
  begin
      PRFORMID.delete;

      declare
        TYPE ctype__ is REF CURSOR;
        c__ ctype__;
      begin
-- Generated SELECT code. Use (i__) to access fields values.
OPEN c__ FOR
select f.formid rformid

from rasd_references r, rasd_forms f

where r.formid = pformid

and r.rlobid = f.lobid

and r.rform = f.form ;
        i__ := 1;
        LOOP
          FETCH c__ INTO
            PRFORMID(i__)
          ;
          exit when c__%notfound;
           if c__%rowcount >=  1 then
-- Generated code for setting lock value based on fiels checked Locked (combination with ON_LOCK trigger). Use (i__) to access fields values.


            exit when i__ =1;
            i__ := i__ + 1;
          end if;
        END LOOP;
         if c__%rowcount <  1 then
          PRFORMID.delete(1);
          PSESSSTORAGEENABLED.delete(1);
          i__ := 0;
        end if;
        CLOSE c__;
      end;
      pclear_P(PRFORMID.count);
  null; end;
  procedure pselect_B10 is
    i__ pls_integer;
  begin
      B10FORM.delete;
      B10LABEL.delete;

      declare
        TYPE ctype__ is REF CURSOR;
        c__ ctype__;
      begin
-- Generated SELECT code. Use (i__) to access fields values.
OPEN c__ FOR
select f.form , f.label

from rasd_references r, rasd_forms f

where (r.rlobid, r.rform) in (

select r.rlobid, r.rform

from rasd_references r

where r.formid = pformid

)

and r.formid = f.formid

order by 1;
        i__ := 1;
        LOOP
          FETCH c__ INTO
            B10FORM(i__)
           ,B10LABEL(i__)
          ;
          exit when c__%notfound;
           if c__%rowcount >=  1 then
-- Generated code for setting lock value based on fiels checked Locked (combination with ON_LOCK trigger). Use (i__) to access fields values.


            exit when i__ =0;
            i__ := i__ + 1;
          end if;
        END LOOP;
         if c__%rowcount <  1 then
          B10FORM.delete(1);
          B10LABEL.delete(1);
          i__ := 0;
        end if;
        CLOSE c__;
      end;
      pclear_B10(B10FORM.count);
  null; end;
  procedure pselect_B20 is
    i__ pls_integer;
  begin
      B20FORM.delete;
      B20LABEL.delete;

      declare
        TYPE ctype__ is REF CURSOR;
        c__ ctype__;
      begin
-- Generated SELECT code. Use (i__) to access fields values.
OPEN c__ FOR
select f.form , f.label

from rasd_forms f

where (formid) in (

select x.formid

from rasd_forms f, rasd_references x

where f.formid = pformid

  and f.lobid = x.rlobid

  and f.form = x.rform

)

order by 1 ;
        i__ := 1;
        LOOP
          FETCH c__ INTO
            B20FORM(i__)
           ,B20LABEL(i__)
          ;
          exit when c__%notfound;
           if c__%rowcount >=  1 then
-- Generated code for setting lock value based on fiels checked Locked (combination with ON_LOCK trigger). Use (i__) to access fields values.


            exit when i__ =0;
            i__ := i__ + 1;
          end if;
        END LOOP;
         if c__%rowcount <  1 then
          B20FORM.delete(1);
          B20LABEL.delete(1);
          i__ := 0;
        end if;
        CLOSE c__;
      end;
      pclear_B20(B20FORM.count);
  null; end;
  procedure pselect is
  begin

    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
      pselect_P;
    end if;
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
      pselect_B10;
    end if;
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
      pselect_B20;
    end if;

  null;
 end;
  procedure pcommit_P is
  begin
    for i__ in 1..PRFORMID.count loop
-- Validating field values before DML. Use (i__) to access fields values.
      if 1=2 then --INSERT
      null; else -- UPDATE or DELETE;
-- Generated code for lock value based on fields checked Locked (combination with ON_LOCK_VALUE trigger). Use (i__) to access fields values.

-- Generated code for mandatory statment based on fiels checked Mandatory. Use (i__) to access fields values.

      null; end if;
    null; end loop;
  null; end;
  procedure pcommit_B10 is
  begin
    for i__ in 1..B10FORM.count loop
-- Validating field values before DML. Use (i__) to access fields values.
      if 1=2 then --INSERT
      null; else -- UPDATE or DELETE;
-- Generated code for lock value based on fields checked Locked (combination with ON_LOCK_VALUE trigger). Use (i__) to access fields values.

-- Generated code for mandatory statment based on fiels checked Mandatory. Use (i__) to access fields values.

      null; end if;
    null; end loop;
  null; end;
  procedure pcommit_B20 is
  begin
    for i__ in 1..B20FORM.count loop
-- Validating field values before DML. Use (i__) to access fields values.
      if 1=2 then --INSERT
      null; else -- UPDATE or DELETE;
-- Generated code for lock value based on fields checked Locked (combination with ON_LOCK_VALUE trigger). Use (i__) to access fields values.

-- Generated code for mandatory statment based on fiels checked Mandatory. Use (i__) to access fields values.

      null; end if;
    null; end loop;
  null; end;
  procedure pcommit is
  begin

    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       pcommit_P;
    end if;
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       pcommit_B10;
    end if;
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       pcommit_B20;
    end if;
--<post_commit formid="93" blockid="">
declare

     n number;

    begin

     select count(*) into n from rasd_references where formid = pformid;



     if n > 0 and PRFORMID(1) is null then



       message := RASDI_TRNSLT.text('You would like to delete referenced form. Would you like to delete referenced data or unlink referenced data! ', lang);



       message :=  message ||  RASDI_TRNSLT.text('Delete', lang)||'<input type="radio" name="UNLINKDELETE" value="DEL"/> / ';

       message :=  message ||  RASDI_TRNSLT.text('Unlink', lang)||'<input type="radio" name="UNLINKDELETE" value="UNL"/> / ';

       message :=  message ||  RASDI_TRNSLT.text('No change', lang)||'<input type="radio" name="UNLINKDELETE" value=""/>.';



--rlog('FFF"'||UNLINKDELETE||'"');

       if UNLINKDELETE is not null then

--rlog('ZZ'||UNLINKDELETE);

--rlog( to_char(instr(UNLINKDELETE,'DEL'))  );

         if  UNLINKDELETE = 'DEL' then

--rlog('ZZ3'||UNLINKDELETE);



             rasdc_library.deleteFormRefData(pformid);

             message := RASDI_TRNSLT.text('Referenced data is deleted.',lang);

         elsif UNLINKDELETE = 'UNL' then

--rlog('ZZ4'||UNLINKDELETE);

             rasdc_library.unlinkFormRefData(pformid);

             message := RASDI_TRNSLT.text('Referenced data is unlinked.',lang);

         end if;

           delete from rasd_references where formid = pformid;

           update rasd_forms f set f.referenceyn = 'N' where f.formid = pformid;



       end if;



     else

--rlog('YY4'||UNLINKDELETE);

      RASDC_LIBRARY.deleteformrefdata(pformid);

      delete from rasd_references where formid = pformid;



      for r in (select * from rasd_forms where formid = PRFORMID(1)) loop

        insert into rasd_references (formid, rlobid, rform) values (pformid, r.lobid, r.form);

        update rasd_forms f set f.referenceyn = 'Y' where f.formid = pformid;

        RASDC_LIBRARY.copyformrefdata( r.formid , pformid);

      end loop;



     end if;





     commit;

      null;

    end;









update RASD_FORMS set change = sysdate where formid = PFORMID;




--</post_commit>
  null;
  end;
  procedure formgen_js is
  begin
  --Input parameter in JS: PFORM
  --Input parameter in JS: PFORMID
    htp.p('function js_Slov$RfForms(pvalue, pobjectname) {');
      htp.p(' eval(''val_''+pobjectname+''="''+pvalue.replace(/"/g, "&#34;")+''"'');');
      htp.p(' var x = document.getElementById(pobjectname+''_RASD''); ');
    begin
    for r__ in (
--<lovsql formid="93" linkid="lov$RfForms">
select '' id, '' label, 1 vr from dual

 union all

select distinct to_char(f.formid) id , f.form||' ('||c2.application||')' label, 2

from rasd_forms f, rasd_forms_compiled c1,  rasd_forms_compiled c2

where c1.formid = pformid

  and c1.lobid = c2.lobid

  and c1.engineid = c2.engineid

--  and c2.compileyn = 'Y'

  and c2.formid = f.formid

  and f.formid <> pformid

order by 3, 2
--</lovsql>
    ) loop
      htp.p('  var option = document.createElement("option"); option.value="'||replace(replace(r__.id,'''','\'''),'"','\"')||'"; option.text = "'||replace(replace(r__.label,'''','\'''),'"','\"')||'"; option.selected = ((pvalue=='''||replace(replace(r__.id,'''','\'''),'"','\"')||''')?'' selected '':''''); x.append(option);');
    end loop;
    exception when others then
      raise_application_error('-20000','Error in lov$RfForms: '||sqlerrm);
    end;
    htp.p('}');
    htp.p('function cMFP() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('function cMFB10() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('function cMFB20() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('function cMF() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
  end;
  procedure poutput is
    iB10 pls_integer;
    iB20 pls_integer;
  function ShowFieldCOMPID return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldCOMPILEALL return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldERROR return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONBCK return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONCLR return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONCOMPILE return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONFWD return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONPREV return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONRES return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONSAVE return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONSRC return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldHINTCONTENT return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldMESSAGE return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldPFORM return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldUNLINKDELETE return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldVLOB return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldVUSER return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldWARNING return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowBlockB10_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowBlockB20_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowBlockP_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  procedure js_Slov$RfForms(value varchar2, name varchar2 default null) is
  begin
    htp.p('<script language="JavaScript">');
    htp.p('js_Slov$RfForms('''||replace(replace(value,'''','\'''),'"','\"')||''','''||name||''');');
    htp.p('</script>');
  end;
procedure output_B10_DIV is begin htp.p('');  if  ShowBlockB10_DIV  then
htp.prn('<div  id="B10_DIV" class="rasdblock"><div>
<caption><div id="B10_LAB" class="labelblock">'|| RASDI_TRNSLT.text('Forms referenced to the same reference:',lang)||'</div></caption><table border="1" id="B10_TABLE" class="rasdTableN display"><thead><tr><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10FORM"><span id="B10FORM_LAB" class="label">'|| showLabel('Form','',0) ||'</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10LABEL"><span id="B10LABEL_LAB" class="label">'|| showLabel('Name','',0) ||'</span></td></tr></thead>'); for iB10 in 1..B10FORM.count loop
htp.prn('<tr id="B10_BLOCK_'||iB10||'"><td class="rasdTxB10FORM rasdTxTypeC" id="rasdTxB10FORM_'||iB10||'"><font id="B10FORM_'||iB10||'_RASD" class="rasdFont">'||B10FORM(iB10)||'</font></td><td class="rasdTxB10LABEL rasdTxTypeC" id="rasdTxB10LABEL_'||iB10||'"><font id="B10LABEL_'||iB10||'_RASD" class="rasdFont">'||B10LABEL(iB10)||'</font></td></tr>'); end loop;
htp.prn('</table></div></div>');  end if;
htp.prn(''); end;
procedure output_B20_DIV is begin htp.p('');  if  ShowBlockB20_DIV  then
htp.prn('<div  id="B20_DIV" class="rasdblock"><div>
<caption><div id="B20_LAB" class="labelblock">'|| RASDI_TRNSLT.text('Forms referenced to this form:',lang)||'</div></caption><table border="1" id="B20_TABLE" class="rasdTableN display"><thead><tr><td class="rasdTxLab rasdTxLabBlockB20" id="rasdTxLabB20FORM"><span id="B20FORM_LAB" class="label">'|| showLabel('Form','',0) ||'</span></td><td class="rasdTxLab rasdTxLabBlockB20" id="rasdTxLabB20LABEL"><span id="B20LABEL_LAB" class="label">'|| showLabel('Name','',0) ||'</span></td></tr></thead>'); for iB20 in 1..B20FORM.count loop
htp.prn('<tr id="B20_BLOCK_'||iB20||'"><td class="rasdTxB20FORM rasdTxTypeC" id="rasdTxB20FORM_'||iB20||'"><font id="B20FORM_'||iB20||'_RASD" class="rasdFont">'||B20FORM(iB20)||'</font></td><td class="rasdTxB20LABEL rasdTxTypeC" id="rasdTxB20LABEL_'||iB20||'"><font id="B20LABEL_'||iB20||'_RASD" class="rasdFont">'||B20LABEL(iB20)||'</font></td></tr>'); end loop;
htp.prn('</table></div></div>');  end if;
htp.prn(''); end;
procedure output_P_DIV is begin htp.p('');  if  ShowBlockP_DIV  then
htp.prn('<div  id="P_DIV" class="rasdblock"><div>
<caption><div id="P_LAB" class="labelblock"></div></caption>
<table border="0" id="P_TABLE"><tr></tr><tr id="P_BLOCK_1"><td class="rasdTxLab rasdTxLabBlockP" id="rasdTxLabPRFORMID">');  if PRFORMID#SET(1).visible then
htp.prn('<span id="PRFORMID_LAB" class="label"></span>');  end if;
htp.prn('</td><td class="rasdTxPRFORMID rasdTxTypeC" id="rasdTxPRFORMID_1">');  if PRFORMID#SET(1).visible then
htp.prn('<select name="PRFORMID_1" ID="PRFORMID_1_RASD" class="rasdSelect ');  if PRFORMID#SET(1).error is not null then htp.p(' errorField'); end if;
htp.prn('');  if PRFORMID#SET(1).info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(PRFORMID#SET(1).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(PRFORMID#SET(1).custom , instr(upper(PRFORMID#SET(1).custom),'CLASS="')+7 , instr(upper(PRFORMID#SET(1).custom),'"',instr(upper(PRFORMID#SET(1).custom),'CLASS="')+8)-instr(upper(PRFORMID#SET(1).custom),'CLASS="')-7) ); end if;
htp.prn('"');  if PRFORMID#SET(1).readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if PRFORMID#SET(1).disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if PRFORMID#SET(1).required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||PRFORMID#SET(1).custom);
htp.prn('');  if PRFORMID#SET(1).error is not null then htp.p(' title="'||PRFORMID#SET(1).error||'"'); end if;
htp.prn('');  if PRFORMID#SET(1).info is not null then htp.p(' title="'||PRFORMID#SET(1).info||'"'); end if;
htp.prn('>'); js_Slov$RfForms(PRFORMID(1),'PRFORMID_1');
htp.prn('</select>');  end if;
htp.prn('</td><span id="" value="1" name="" class="hiddenRowItems"><input name="SESSSTORAGEENABLED_1" id="SESSSTORAGEENABLED_1_RASD" type="hidden" value="'||PSESSSTORAGEENABLED(1)||'"/>
</span></tr></table></div></div>');  end if;
htp.prn(''); end;
  begin
if set_session_block__ is not null then  execute immediate set_session_block__;  end if;
    htp.prn('<html>
<head>');
htpClob(rasd_client.getHtmlJSLibrary('HEAD',''|| rasdc_library.formName(PFORMID, LANG) ||''));
htp.p('');
htp.p('<script type="text/javascript">');
formgen_js;
htp.p('</script>');
htpClob(FORM_UIHEAD);
htp.p('<style type="text/css">');
htpClob(FORM_CSS);
htp.p('</style><script type="text/javascript">'); htpClob(FORM_JS); htp.p('</script>');

htp.prn('</head>
<body><div id="RASDC2_REFERENCES_LAB" class="rasdFormLab">'|| rasd_client.getHtmlHeaderDataTable('RASDC2_REFERENCES_LAB',''|| rasdc_library.formName(PFORMID, LANG) ||'') ||'     </div><div id="RASDC2_REFERENCES_MENU" class="rasdFormMenu">'|| rasd_client.getHtmlMenuList('RASDC2_REFERENCES_MENU') ||'     </div>
<form name="RASDC2_REFERENCES" method="post" action="?"><div id="RASDC2_REFERENCES_DIV" class="rasdForm"><div id="RASDC2_REFERENCES_HEAD" class="rasdFormHead"><input name="RECNUMB10" id="RECNUMB10_RASD" type="hidden" value="'||ltrim(to_char(RECNUMB10))||'"/>
<input name="RECNUMB20" id="RECNUMB20_RASD" type="hidden" value="'||ltrim(to_char(RECNUMB20))||'"/>
<input name="RECNUMP" id="RECNUMP_RASD" type="hidden" value="'||ltrim(to_char(RECNUMP))||'"/>
<input name="ACTION" id="ACTION_RASD" type="hidden" value="'||ACTION||'"/>
<input name="PAGE" id="PAGE_RASD" type="hidden" value="'||ltrim(to_char(PAGE))||'"/>
<input name="LANG" id="LANG_RASD" type="hidden" value="'||LANG||'"/>
<input name="PFORMID" id="PFORMID_RASD" type="hidden" value="'||PFORMID||'"/>
');
if  ShowFieldGBUTTONSAVE  then
htp.prn('');  if GBUTTONSAVE#SET.visible then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONSAVE" id="GBUTTONSAVE_RASD" type="button" value="'||GBUTTONSAVE||'" class="rasdButton');  if GBUTTONSAVE#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONSAVE#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONSAVE#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONSAVE#SET.custom),'"',instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONSAVE#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONSAVE#SET.custom);
htp.prn('');  if GBUTTONSAVE#SET.error is not null then htp.p(' title="'||GBUTTONSAVE#SET.error||'"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.info is not null then htp.p(' title="'||GBUTTONSAVE#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONCOMPILE  then
htp.prn('');  if GBUTTONCOMPILE#SET.visible then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONCOMPILE" id="GBUTTONCOMPILE_RASD" type="button" value="'||GBUTTONCOMPILE||'" class="rasdButton');  if GBUTTONCOMPILE#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONCOMPILE#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),'"',instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONCOMPILE#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONCOMPILE#SET.custom);
htp.prn('');  if GBUTTONCOMPILE#SET.error is not null then htp.p(' title="'||GBUTTONCOMPILE#SET.error||'"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.info is not null then htp.p(' title="'||GBUTTONCOMPILE#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONRES  then
htp.prn('');  if GBUTTONRES#SET.visible then
htp.prn('<input name="GBUTTONRES" id="GBUTTONRES_RASD" type="reset" value="'||GBUTTONRES||'" class="rasdButton');  if GBUTTONRES#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONRES#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONRES#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONRES#SET.custom),'"',instr(upper(GBUTTONRES#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONRES#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONRES#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONRES#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONRES#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONRES#SET.custom);
htp.prn('');  if GBUTTONRES#SET.error is not null then htp.p(' title="'||GBUTTONRES#SET.error||'"'); end if;
htp.prn('');  if GBUTTONRES#SET.info is not null then htp.p(' title="'||GBUTTONRES#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONPREV  then
htp.prn('');  if GBUTTONPREV#SET.visible then
htp.prn('<span id="GBUTTONPREV_RASD"');  if GBUTTONPREV#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONPREV#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONPREV#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONPREV#SET.custom);
htp.prn('');  if GBUTTONPREV#SET.error is not null then htp.p(' title="'||GBUTTONPREV#SET.error||'"'); end if;
htp.prn('');  if GBUTTONPREV#SET.info is not null then htp.p(' title="'||GBUTTONPREV#SET.info||'"'); end if;
htp.prn('>');  end if;
htp.prn('');  htp.p( '<input type=button class="rasdButton" value="' ||

                   RASDI_TRNSLT.text('Preview', lang) || '" ' ||

                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,

                                'disabled="disabled" style="background-color: red;" title="' ||RASDI_TRNSLT.text('Program has ERRORS!',lang)||'" ',

                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,

                                     'style="background-color: orange;" title="' ||RASDI_TRNSLT.text('Programa has changes. Compile it.',lang) ||

                                     '" onclick="x=window.open(''!' ||pform ||'.webclient'','''','''')" '

									 ,

                                     'style="background-color: green;" onclick="x=window.open(''!' ||pform ||'.webclient'','''','''')" '

                                )

			       ) || '>'

);
htp.prn('</span>');  end if;
htp.prn('');
if  ShowFieldHINTCONTENT  then
htp.prn('<span id="HINTCONTENT_RASD">');  htp.p('<div style="display: none;">');

htp.p(rasdc_hints.getHint(replace(this_form,'2','')||'_DIALOG',lang));

htp.p('</div>');
htp.prn('</span>');  end if;
htp.prn('</div><div id="RASDC2_REFERENCES_RESPONSE" class="rasdFormResponse"><div id="RASDC2_REFERENCES_ERROR" class="rasdFormMessage error"><font id="ERROR_RASD" class="rasdFont">'||ERROR||'</font></div><div id="RASDC2_REFERENCES_WARNING" class="rasdFormMessage warning"><font id="WARNING_RASD" class="rasdFont">'||WARNING||'</font></div><div id="RASDC2_REFERENCES_MESSAGE" class="rasdFormMessage"><font id="MESSAGE_RASD" class="rasdFont">'||MESSAGE||'</font></div></div><div id="RASDC2_REFERENCES_BODY" class="rasdFormBody">'); output_P_DIV; htp.p(''); output_B10_DIV; htp.p(''); output_B20_DIV; htp.p('</div><div id="RASDC2_REFERENCES_FOOTER" class="rasdFormFooter">');
if  ShowFieldGBUTTONSAVE  then
htp.prn('');  if GBUTTONSAVE#SET.visible then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONSAVE" id="GBUTTONSAVE_RASD" type="button" value="'||GBUTTONSAVE||'" class="rasdButton');  if GBUTTONSAVE#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONSAVE#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONSAVE#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONSAVE#SET.custom),'"',instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONSAVE#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONSAVE#SET.custom);
htp.prn('');  if GBUTTONSAVE#SET.error is not null then htp.p(' title="'||GBUTTONSAVE#SET.error||'"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.info is not null then htp.p(' title="'||GBUTTONSAVE#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONCOMPILE  then
htp.prn('');  if GBUTTONCOMPILE#SET.visible then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONCOMPILE" id="GBUTTONCOMPILE_RASD" type="button" value="'||GBUTTONCOMPILE||'"
 class="rasdButton');  if GBUTTONCOMPILE#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONCOMPILE#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),'"',instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONCOMPILE#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONCOMPILE#SET.custom);
htp.prn('');  if GBUTTONCOMPILE#SET.error is not null then htp.p(' title="'||GBUTTONCOMPILE#SET.error||'"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.info is not null then htp.p(' title="'||GBUTTONCOMPILE#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONRES  then
htp.prn('');  if GBUTTONRES#SET.visible then
htp.prn('<input name="GBUTTONRES" id="GBUTTONRES_RASD" type="reset" value="'||GBUTTONRES||'" class="rasdButton');  if GBUTTONRES#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONRES#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONRES#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONRES#SET.custom),'"',instr(upper(GBUTTONRES#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONRES#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONRES#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONRES#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONRES#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONRES#SET.custom);
htp.prn('');  if GBUTTONRES#SET.error is not null then htp.p(' title="'||GBUTTONRES#SET.error||'"'); end if;
htp.prn('');  if GBUTTONRES#SET.info is not null then htp.p(' title="'||GBUTTONRES#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONPREV  then
htp.prn('');  if GBUTTONPREV#SET.visible then
htp.prn('<span id="GBUTTONPREV_RASD"');  if GBUTTONPREV#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONPREV#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONPREV#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONPREV#SET.custom);
htp.prn('');  if GBUTTONPREV#SET.error is not null then htp.p(' title="'||GBUTTONPREV#SET.error||'"'); end if;
htp.prn('');  if GBUTTONPREV#SET.info is not null then htp.p(' title="'||GBUTTONPREV#SET.info||'"'); end if;
htp.prn('>');  end if;
htp.prn('');  htp.p( '<input type=button class="rasdButton" value="' ||

                   RASDI_TRNSLT.text('Preview', lang) || '" ' ||

                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,

                                'disabled="disabled" style="background-color: red;" title="' ||RASDI_TRNSLT.text('Program has ERRORS!',lang)||'" ',

                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,

                                     'style="background-color: orange;" title="' ||RASDI_TRNSLT.text('Programa has changes. Compile it.',lang) ||

                                     '" onclick="x=window.open(''!' ||pform ||'.webclient'','''','''')" '

									 ,

                                     'style="background-color: green;" onclick="x=window.open(''!' ||pform ||'.webclient'','''','''')" '

                                )

			       ) || '>'

);
htp.prn('</span>');  end if;
htp.prn('');
if  ShowFieldHINTCONTENT  then
htp.prn('<span id="HINTCONTENT_RASD">');  htp.p('<div style="display: none;">');

htp.p(rasdc_hints.getHint(replace(this_form,'2','')||'_DIALOG',lang));

htp.p('</div>');
htp.prn('</span>');  end if;
htp.prn('</div></div></form><div id="RASDC2_REFERENCES_BOTTOM" class="rasdFormBottom">'|| rasd_client.getHtmlFooter(version , substr('RASDC2_REFERENCES_BOTTOM',1,instr('RASDC2_REFERENCES_BOTTOM', '_',-1)-1) , '') ||'</div></body></html>
    ');
  null; end;
  function poutputrest return clob is
    v_firstrow__ boolean;
    v_clob__ clob;
    procedure htpp(v_str varchar2) is
    begin
      v_clob__ := v_clob__ || v_str;
    end;
    function escapeRest(v_str varchar2) return varchar2 is
    begin
      return replace(v_str,'"','&quot;');
    end;
    function escapeRest(v_str clob) return clob is
    begin
      return replace(v_str,'"','&quot;');
    end;
  function ShowBlockB10_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowBlockB20_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  function ShowBlockP_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then
       return true;
    end if;
    return false;
  end;
  begin
if RESTRESTYPE = 'XML' then
    htpp('<?xml version="1.0" encoding="UTF-8"?>');
    htpp('<form name="RASDC2_REFERENCES" version="'||version||'">');
    htpp('<formfields>');
    htpp('<recnumb10>'||RECNUMB10||'</recnumb10>');
    htpp('<recnumb20>'||RECNUMB20||'</recnumb20>');
    htpp('<recnump>'||RECNUMP||'</recnump>');
    htpp('<action><![CDATA['||ACTION||']]></action>');
    htpp('<page>'||PAGE||'</page>');
    htpp('<lang><![CDATA['||LANG||']]></lang>');
    htpp('<pformid><![CDATA['||PFORMID||']]></pformid>');
    htpp('<gbuttonsave><![CDATA['||GBUTTONSAVE||']]></gbuttonsave>');
    htpp('<gbuttoncompile><![CDATA['||GBUTTONCOMPILE||']]></gbuttoncompile>');
    htpp('<gbuttonres><![CDATA['||GBUTTONRES||']]></gbuttonres>');
    htpp('<gbuttonprev><![CDATA['||GBUTTONPREV||']]></gbuttonprev>');
    htpp('<error><![CDATA['||ERROR||']]></error>');
    htpp('<message><![CDATA['||MESSAGE||']]></message>');
    htpp('<warning><![CDATA['||WARNING||']]></warning>');
    htpp('<hintcontent><![CDATA['||HINTCONTENT||']]></hintcontent>');
    htpp('</formfields>');
    if ShowBlockp_DIV then
    htpp('<p>');
  for i__ in 1..
PRFORMID
.count loop
    htpp('<element>');
    htpp('<prformid><![CDATA['||PRFORMID(i__)||']]></prformid>');
    htpp('<sessstorageenabled><![CDATA['||PSESSSTORAGEENABLED(i__)||']]></sessstorageenabled>');
    htpp('</element>');
  end loop;
  htpp('</p>');
  end if;
    if ShowBlockb10_DIV then
    htpp('<b10>');
  for i__ in 1..
B10FORM
.count loop
    htpp('<element>');
    htpp('<b10form><![CDATA['||B10FORM(i__)||']]></b10form>');
    htpp('<b10label><![CDATA['||B10LABEL(i__)||']]></b10label>');
    htpp('</element>');
  end loop;
  htpp('</b10>');
  end if;
    if ShowBlockb20_DIV then
    htpp('<b20>');
  for i__ in 1..
B20FORM
.count loop
    htpp('<element>');
    htpp('<b20form><![CDATA['||B20FORM(i__)||']]></b20form>');
    htpp('<b20label><![CDATA['||B20LABEL(i__)||']]></b20label>');
    htpp('</element>');
  end loop;
  htpp('</b20>');
  end if;
    htpp('</form>');
else
    htpp('{"form":{"@name":"RASDC2_REFERENCES","@version":"'||version||'",' );
    htpp('"formfields": {');
    htpp('"recnumb10":"'||RECNUMB10||'"');
    htpp(',"recnumb20":"'||RECNUMB20||'"');
    htpp(',"recnump":"'||RECNUMP||'"');
    htpp(',"action":"'||escapeRest(ACTION)||'"');
    htpp(',"page":"'||PAGE||'"');
    htpp(',"lang":"'||escapeRest(LANG)||'"');
    htpp(',"pformid":"'||escapeRest(PFORMID)||'"');
    htpp(',"gbuttonsave":"'||escapeRest(GBUTTONSAVE)||'"');
    htpp(',"gbuttoncompile":"'||escapeRest(GBUTTONCOMPILE)||'"');
    htpp(',"gbuttonres":"'||escapeRest(GBUTTONRES)||'"');
    htpp(',"gbuttonprev":"'||escapeRest(GBUTTONPREV)||'"');
    htpp(',"error":"'||escapeRest(ERROR)||'"');
    htpp(',"message":"'||escapeRest(MESSAGE)||'"');
    htpp(',"warning":"'||escapeRest(WARNING)||'"');
    htpp(',"hintcontent":"'||escapeRest(HINTCONTENT)||'"');
    htpp('},');
    if ShowBlockp_DIV then
    htpp('"p":[');
  v_firstrow__ := true;
  for i__ in 1..
PRFORMID
.count loop
    if v_firstrow__ then
     htpp('{');
     v_firstrow__ := false;
    else
     htpp(',{');
    end if;
    htpp('"prformid":"'||escapeRest(PRFORMID(i__))||'"');
    htpp(',"sessstorageenabled":"'||escapeRest(PSESSSTORAGEENABLED(i__))||'"');
    htpp('}');
  end loop;
    htpp(']');
  else
    htpp('"p":[]');
  end if;
    if ShowBlockb10_DIV then
    htpp(',"b10":[');
  v_firstrow__ := true;
  for i__ in 1..
B10FORM
.count loop
    if v_firstrow__ then
     htpp('{');
     v_firstrow__ := false;
    else
     htpp(',{');
    end if;
    htpp('"b10form":"'||escapeRest(B10FORM(i__))||'"');
    htpp(',"b10label":"'||escapeRest(B10LABEL(i__))||'"');
    htpp('}');
  end loop;
    htpp(']');
  else
    htpp(',"b10":[]');
  end if;
    if ShowBlockb20_DIV then
    htpp(',"b20":[');
  v_firstrow__ := true;
  for i__ in 1..
B20FORM
.count loop
    if v_firstrow__ then
     htpp('{');
     v_firstrow__ := false;
    else
     htpp(',{');
    end if;
    htpp('"b20form":"'||escapeRest(B20FORM(i__))||'"');
    htpp(',"b20label":"'||escapeRest(B20LABEL(i__))||'"');
    htpp('}');
  end loop;
    htpp(']');
  else
    htpp(',"b20":[]');
  end if;
    htpp('}}');
end if;
return v_clob__;
null; end;
procedure poutputrest is
begin
if RESTRESTYPE = 'XML' then
    OWA_UTIL.mime_header('text/xml', FALSE,'utf-8');
    OWA_UTIL.http_header_close;
else
    OWA_UTIL.mime_header('application/json', FALSE ,'utf-8');
    OWA_UTIL.http_header_close;
end if;
htpclob(poutputrest);
end;
procedure webclient(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin
  rasd_client.secCheckCredentials(  name_array , value_array );

  -- The program execution sequence based on  ACTION defined.
  psubmit(name_array ,value_array);
  rasd_client.secCheckPermission('RASDC2_REFERENCES',ACTION);
  if ACTION is null then null;
    RECNUMP := 1;
    RECNUMB10 := 1;
    RECNUMB20 := 1;
    pselect;
    poutput;
  elsif ACTION = GBUTTONBCK then     pselect;
    poutput;
  elsif ACTION = GBUTTONFWD then     pselect;
    poutput;
  elsif ACTION = GBUTTONSRC then     RECNUMP := 1;
    RECNUMB10 := 1;
    RECNUMB20 := 1;
    pselect;
    poutput;
  elsif ACTION = GBUTTONSAVE then     pcommit;
    pselect;
    --if MESSAGE is null then
    --MESSAGE := 'Form is changed.';
    --end if;
    poutput;
  elsif ACTION = GBUTTONCLR then     pclear;
    poutput;
  end if;

--<POST_ACTION formid="93" blockid="">
if action = GBUTTONCOMPILE then



if PRFORMID(1) is not null then

message := RASDI_TRNSLT.text('You can multi compile only templete form.',lang);

else

if COMPILEALL is null then



message := RASDI_TRNSLT.text('Do you want to compile all forms referenced to this form? Check <input name="COMPILEALL" value="Y" type="checkbox"/> and Compile again.',lang);



else



if COMPILEALL = 'Y' then



message := RASDI_TRNSLT.text('Compiled forms: ',lang);



declare

  sp varchar2(1000);

begin

for r in (select f.form , f.label, f.formid

from rasd_forms f

where (formid) in (

select x.formid

from rasd_forms f, rasd_references x

where f.formid = pformid

  and f.lobid = x.rlobid

  and f.form = x.rform

)

order by 1 ) loop



message := message ||'<br/> '||r.form||' :  ';



compile(r.formid , r.form , lang ,  sp , ''  , compid);

message := message || sp;



end loop;



end;



end if;



end if;

end if;

pselect;

poutput;



end if;
--</POST_ACTION>
    pLog;
exception
  when rasd_client.e_finished then pLog;
  when others then
    htp.p('<html>
<head>');
htpClob(rasd_client.getHtmlJSLibrary('HEAD',''|| rasdc_library.formName(PFORMID, LANG) ||''));
htp.p('');
htp.p('<script type="text/javascript">');
formgen_js;
htp.p('</script>');
htpClob(FORM_UIHEAD);
htp.p('<style type="text/css">');
htpClob(FORM_CSS);
htp.p('</style><script type="text/javascript">');  htp.p('</script>');

htp.prn('</head><body><div id="RASDC2_REFERENCES_LAB" class="rasdFormLab">'|| rasd_client.getHtmlHeaderDataTable('RASDC2_REFERENCES_LAB',''|| rasdc_library.formName(PFORMID, LANG) ||'') ||'     </div><div class="rasdForm"><div class="rasdFormHead"><input onclick="history.go(-1);" type="button" value="Back" class="rasdButton"></div><div class="rasdHtmlError">  <div class="rasdHtmlErrorText"><div class="rasdHtmlErrorText">'||sqlerrm||'('||sqlcode||')</div></div><div class="rasdHtmlErrorTextDetail">');declare   v_trace varchar2(32000) := DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;   v_nl varchar2(2) := chr(10); begin rlog('ERROR:'||v_trace); htp.p ( 'Error trace'||':'||'<br/>'|| replace(v_trace, v_nl ,'<br/>'));htp.p ( '</div><div class="rasdHtmlErrorTextDetail">'||'Error stack'||':'||'<br/>'|| replace(DBMS_UTILITY.FORMAT_ERROR_STACK, v_nl ,'<br/>'));rlog('ERROR:'||DBMS_UTILITY.FORMAT_ERROR_STACK); htp.p('</div>');rlog('ERROR:...'); declare   v_line  number;  v_x varchar2(32000); begin v_x := substr(v_trace,1,instr(v_trace, v_nl )-1 );  v_line := substr(v_x,instr(v_x,' ',-1));for r in  (select line, text from user_source s where s.name = 'RASDC2_REFERENCES' and line > v_line-5 and line < v_line+5 ) loop rlog('ERROR:'||r.line||' - '||r.text); end loop;  rlog('ERROR:...'); exception when others then null;end;end;htp.p('</div><div class="rasdFormFooter"><input onclick="history.go(-1);" type="button" value="Back" class="rasdButton">'|| rasd_client.getHtmlFooter(version , substr('RASDC2_REFERENCES_FOOTER',1,instr('RASDC2_REFERENCES_FOOTER', '_',-1)-1) , '') ||'</div></div></body></html>    ');
    pLog;
end;
procedure main(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin
  rasd_client.secCheckCredentials(  name_array , value_array );

  -- The program execution sequence based on  ACTION defined.
  psubmit(name_array ,value_array);
  rasd_client.secCheckPermission('RASDC2_REFERENCES',ACTION);
  if ACTION = GBUTTONSAVE then     pselect;
    pcommit;
  end if;

--<POST_ACTION formid="93" blockid="">
if action = GBUTTONCOMPILE then



if PRFORMID(1) is not null then

message := RASDI_TRNSLT.text('You can multi compile only templete form.',lang);

else

if COMPILEALL is null then



message := RASDI_TRNSLT.text('Do you want to compile all forms referenced to this form? Check <input name="COMPILEALL" value="Y" type="checkbox"/> and Compile again.',lang);



else



if COMPILEALL = 'Y' then



message := RASDI_TRNSLT.text('Compiled forms: ',lang);



declare

  sp varchar2(1000);

begin

for r in (select f.form , f.label, f.formid

from rasd_forms f

where (formid) in (

select x.formid

from rasd_forms f, rasd_references x

where f.formid = pformid

  and f.lobid = x.rlobid

  and f.form = x.rform

)

order by 1 ) loop



message := message ||'<br/> '||r.form||' :  ';



compile(r.formid , r.form , lang ,  sp , ''  , compid);

message := message || sp;



end loop;



end;



end if;



end if;

end if;

pselect;

poutput;



end if;
--</POST_ACTION>
-- Error handler for the main program.
 exception
  when rasd_client.e_finished then null;

end;
procedure rest(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin
  rasd_client.secCheckCredentials(  name_array , value_array );

  -- The program execution sequence based on  ACTION defined.
  psubmitrest(name_array ,value_array);
  rasd_client.secCheckPermission('RASDC2_REFERENCES',ACTION);
  if ACTION is null then null;
    RECNUMP := 1;
    RECNUMB10 := 1;
    RECNUMB20 := 1;
    pselect;
    poutputrest;
  elsif ACTION = GBUTTONBCK then     pselect;
    poutputrest;
  elsif ACTION = GBUTTONFWD then     pselect;
    poutputrest;
  elsif ACTION = GBUTTONSRC or ACTION is null  then     RECNUMP := 1;
    RECNUMB10 := 1;
    RECNUMB20 := 1;
    pselect;
    poutputrest;
  elsif ACTION = GBUTTONSAVE then     pcommit;
    pselect;
    --if MESSAGE is null then
    --MESSAGE := 'Form is changed.';
    --end if;
    poutputrest;
  elsif ACTION = GBUTTONCLR then     pclear;
    poutputrest;
  end if;

--<POST_ACTION formid="93" blockid="">
if action = GBUTTONCOMPILE then



if PRFORMID(1) is not null then

message := RASDI_TRNSLT.text('You can multi compile only templete form.',lang);

else

if COMPILEALL is null then



message := RASDI_TRNSLT.text('Do you want to compile all forms referenced to this form? Check <input name="COMPILEALL" value="Y" type="checkbox"/> and Compile again.',lang);



else



if COMPILEALL = 'Y' then



message := RASDI_TRNSLT.text('Compiled forms: ',lang);



declare

  sp varchar2(1000);

begin

for r in (select f.form , f.label, f.formid

from rasd_forms f

where (formid) in (

select x.formid

from rasd_forms f, rasd_references x

where f.formid = pformid

  and f.lobid = x.rlobid

  and f.form = x.rform

)

order by 1 ) loop



message := message ||'<br/> '||r.form||' :  ';



compile(r.formid , r.form , lang ,  sp , ''  , compid);

message := message || sp;



end loop;



end;



end if;



end if;

end if;

pselect;

poutput;



end if;
--</POST_ACTION>
-- Error handler for the rest program.
 exception
  when rasd_client.e_finished then null;
  when others then
if RESTRESTYPE = 'XML' then
    OWA_UTIL.mime_header('text/xml', FALSE,'utf-8');
    OWA_UTIL.http_header_close;
    htp.p('<?xml version="1.0" encoding="UTF-8"?>
<form name="RASDC2_REFERENCES" version="'||version||'">');     htp.p('<error>');     htp.p('  <errorcode>'||sqlcode||'</errorcode>');     htp.p('  <errormessage>'||replace(sqlerrm,'<','&lt;')||'</errormessage>');     htp.p('</error>');     htp.p('</form>'); else
    OWA_UTIL.mime_header('application/json', FALSE,'utf-8');
    OWA_UTIL.http_header_close;
    htp.p('{"form":{"@name":"RASDC2_REFERENCES","@version":"'||version||'",' );     htp.p('"error":{');     htp.p('  "errorcode":"'||sqlcode||'",');     htp.p('  "errormessage":"'||replace(sqlerrm,'"','\"')||'"');     htp.p('}');     htp.p('}}'); end if;

end;
function metadata_xml return cctab is
  v_clob clob := '';
  v_vc cctab;
  begin
 v_vc(1) := '<form><formid>93</formid><form>RASDC2_REFERENCES</form><version>1</version><change>28.02.2024 09/10/44</change><user>RASDDEV</user><label><![CDATA[<%= rasdc_library.formName(PFORMID, LANG) %>]]></label><lobid>RASDDEV</lobid><program>?</program><referenceyn>Y</referenceyn><autodeletehtmlyn>Y</autodeletehtmlyn><autocreaterestyn>Y</autocreaterestyn><autocreatebatchyn>Y</autocreatebatchyn><addmetadatainfoyn>Y</addmetadatainfoyn><compiler><engineid>11</engineid><server>rasd_engine11</server><client>rasd_enginehtml11</client><library>rasd_client</library></compiler><compiledInfo><info><engineid>11</engineid><change>05.04.2023 11/15/08</change><compileyn>Y</compileyn><application>RASD 2.0</application><owner>domen</owner><editor>domen</editor></info></compiledInfo><blocks><block><blockid>B10</blockid><sqltable></sqltable><numrows>0</numrows><emptyrows></emptyrows><dbblockyn>Y</dbblockyn><rowidyn>N</rowidyn><pagingyn>N</pagingyn><clearyn>N</clearyn><sqltext><![CDATA[select f.form , f.label

from rasd_references r, rasd_forms f

where (r.rlobid, r.rform) in (

select r.rlobid, r.rform

from rasd_references r

where r.formid = pformid

)

and r.formid = f.formid

order by 1]]></sqltext><label><![CDATA[<%= RASDI_TRNSLT.text(''Forms referenced to the same reference:'',lang)%>]]></label><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid><fields><field><blockid>B10</blockid><fieldid>FORM</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>10</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10FORM</nameid><label><![CDATA[<%= showLabel(''Form'','''',0) %>]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>LABEL</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>20</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10LABEL</nameid><label><![CDATA[<%= sh';
 v_vc(2) := 'owLabel(''Name'','''',0) %>]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field></fields></block><block><blockid>B20</blockid><sqltable></sqltable><numrows>0</numrows><emptyrows></emptyrows><dbblockyn>Y</dbblockyn><rowidyn>N</rowidyn><pagingyn>N</pagingyn><clearyn>N</clearyn><sqltext><![CDATA[select f.form , f.label

from rasd_forms f

where (formid) in (

select x.formid

from rasd_forms f, rasd_references x

where f.formid = pformid

  and f.lobid = x.rlobid

  and f.form = x.rform

)

order by 1 ]]></sqltext><label><![CDATA[<%= RASDI_TRNSLT.text(''Forms referenced to this form:'',lang)%>]]></label><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid><fields><field><blockid>B20</blockid><fieldid>FORM</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>30</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B20FORM</nameid><label><![CDATA[<%= showLabel(''Form'','''',0) %>]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B20</blockid><fieldid>LABEL</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>40</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B20LABEL</nameid><label><![CDATA[<%= showLabel(''Name'','''',0) %>]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field></fields></block><block><blockid>P</blockid><sqltable></sqltable><numrows>1</numrows><emptyrows></emptyrows><dbblockyn>Y</dbblockyn><rowidyn>N</rowidyn><pagingyn>N</pagingyn><clearyn>N</clearyn><sqltext><![CDATA[select f.formid rformid

from rasd_references r, rasd_forms f

where r.formid = pformid

and r.rlobid = f.lobid

and r.rform = f.form ]]></sqltext><label></label><source>V</source><hiddenyn></hiddenyn';
 v_vc(3) := '><rlobid></rlobid><rform></rform><rblockid></rblockid><fields><field><blockid>P</blockid><fieldid>RFORMID</fieldid><type>C</type><format></format><element>SELECT_</element><hiddenyn></hiddenyn><orderby>-9</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>PRFORMID</nameid><label></label><linkid>lov$RfForms</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>P</blockid><fieldid>SESSSTORAGEENABLED</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>SESSSTORAGEENABLED</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field></fields></block></blocks><fields><field><blockid></blockid><fieldid>ACTION</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>ACTION</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>ACTION</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>COMPID</fieldid><type>N</type><format></format><element></element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>COMPID</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid>';
 v_vc(4) := '<fieldid>COMPILEALL</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>COMPILEALL</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>ERROR</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>50</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>ERROR</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>ERROR</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>GBUTTONBCK</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>10</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[''<<'']]></defaultval><elementyn>N</elementyn><nameid>GBUTTONBCK</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>GBUTTONCLR</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[''GBUTTONCLR'']]></defaultval><elementyn>N</elementyn><nameid>GBUTTONCLR</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONCLR</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>GBUTTONCOMPILE</fieldid><type>C</type><format></format><element>IN';
 v_vc(5) := 'PUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>13</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Compile'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONCOMPILE</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONCOMPILE</rfieldid><includevis>Y</includevis></field><field><blockid></blockid><fieldid>GBUTTONFWD</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>11</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[''>>'']]></defaultval><elementyn>N</elementyn><nameid>GBUTTONFWD</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>GBUTTONPREV</fieldid><type>C</type><format></format><element>PLSQL_</element><hiddenyn></hiddenyn><orderby>15</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Preview'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONPREV</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONPREV</rfieldid><includevis>Y</includevis></field><field><blockid></blockid><fieldid>GBUTTONRES</fieldid><type>C</type><format></format><element>INPUT_RESET</element><hiddenyn></hiddenyn><orderby>14</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Reset'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONRES</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONRES</rfieldid><includevis>Y</includevis></fi';
 v_vc(6) := 'eld><field><blockid></blockid><fieldid>GBUTTONSAVE</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>12</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Save'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONSAVE</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONSAVE</rfieldid><includevis>Y</includevis></field><field><blockid></blockid><fieldid>GBUTTONSRC</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>9</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Search'',LANG)]]></defaultval><elementyn>N</elementyn><nameid>GBUTTONSRC</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONSRC</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>HINTCONTENT</fieldid><type>C</type><format></format><element>PLSQL_</element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>HINTCONTENT</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>HINTCONTENT</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>LANG</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>5</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>LANG</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfie';
 v_vc(7) := 'ldid>LANG</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>MESSAGE</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>51</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>MESSAGE</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>MESSAGE</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>PAGE</fieldid><type>N</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[0]]></defaultval><elementyn>Y</elementyn><nameid>PAGE</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>PAGE</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>PFORM</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>7</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>PFORM</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>PFORM</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>PFORMID</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>6</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>PFORMID</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>PFORMID</rfieldid><includevis>N</includevis></field><field><blockid></blockid>';
 v_vc(8) := '<fieldid>RECNUMB10</fieldid><type>N</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>-10</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[1]]></defaultval><elementyn>Y</elementyn><nameid>RECNUMB10</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>RECNUMB20</fieldid><type>N</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>-2</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[1]]></defaultval><elementyn>Y</elementyn><nameid>RECNUMB20</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>RECNUMP</fieldid><type>N</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>-2</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[1]]></defaultval><elementyn>Y</elementyn><nameid>RECNUMP</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>UNLINKDELETE</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>UNLINKDELETE</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>VLOB</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>81</orderby><pkyn>N';
 v_vc(9) := '</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>VLOB</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>VLOB</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>VUSER</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>80</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>VUSER</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>VUSER</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>WARNING</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>52</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>WARNING</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>WARNING</rfieldid><includevis>N</includevis></field></fields><links><link><linkid>lov$RfForms</linkid><link>REFERENCED FORMS</link><type>S</type><location></location><text><![CDATA[select '''' id, '''' label, 1 vr from dual

 union all

select distinct to_char(f.formid) id , f.form||'' (''||c2.application||'')'' label, 2

from rasd_forms f, rasd_forms_compiled c1,  rasd_forms_compiled c2

where c1.formid = pformid

  and c1.lobid = c2.lobid

  and c1.engineid = c2.engineid

--  and c2.compileyn = ''Y''

  and c2.formid = f.formid

  and f.formid <> pformid

order by 3, 2]]></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params></params></link></links><pages><pagedata><page>0</page><blockid></blockid><fieldid>PF</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid>';
 v_vc(10) := '</blockid><fieldid>ERROR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>ERROR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>MESSAGE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>MESSAGE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>WARNING</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>WARNING</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid>P</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid>P</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid>B10</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid>B10</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid>B20</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid>B20</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONCLR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONCLR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>PFORM</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>PFORM</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata';
 v_vc(11) := '><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONSRC</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONSRC</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONBCK</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONBCK</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONFWD</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONFWD</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONSAVE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONSAVE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONCOMPILE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONCOMPILE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONRES</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONRES</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONPREV</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONPREV</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>COMPID</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></b';
 v_vc(12) := 'lockid><fieldid>COMPID</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>HINTCONTENT</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>HINTCONTENT</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>SHOWHIDECOLS</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>UNLINK</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>UNLINK</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata></pages><triggers><trigger><blockid></blockid><triggerid>FORM_CSS</triggerid><plsql><![CDATA[.rasdTxB10SQLTABLE INPUT{

   width: 300px;

}



.rasdTxPF INPUT{

   width: 300px;

}



.rasdTxB10LABEL INPUT{

   width: 300px;

}



#rasdTxB10VERSION_1 {

   text-align: center;

}



.rasdTxB20ORDERBY {

width: 50px;

}





table.dataTable tbody th, table.dataTable tbody td {

   padding: 2px 2px;

}



.rasdTextC {

    width: 80;

}



.rasdTxB20ELEMENT SELECT {

    width: 100;

}



.rasdTxB20LINKID SELECT {

    width: 100;

}



#rasdTxLabB10FORM {

    width: 400px;

}



#rasdTxLabB20FORM {

    width: 400px;

}



#RASDC2_REFERENCES_MENU ul li:nth-child(1), #RASDC2_REFERENCES_MENU ul li:nth-child(n+3){

 display: none;

}


]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>FORM_JS</triggerid><plsql><![CDATA[$(function() {



//  addSpinner();

//   initRowStatus();

//   transformVerticalTable("B15_TABLE", 4 );

   setShowHideDiv("B30_DIV", true);

//   CheckFieldValue(pid , pname)

//   CheckFieldMandatory(pid , pname)





HighLightRow("referenceBlock", "#aaccf7");





$(".rasdTxB20FORMAT INPUT").attr("maxlength", 30);

$(".rasdTxB20FIELDID INPUT").attr("maxlength", 30);

$(".rasdTxB20ORDERBY INPUT").attr("maxlength", 10);

$(".rasdTxB20DEFAULTVAL INPUT").attr("maxlength", 30);

$(".rasdTxB20NAMEID INPUT").attr("maxl';
 v_vc(13) := 'ength", 30);

$(".rasdTxB20LABEL INPUT").attr("maxlength", 30);





    var newDT1 = JSON.parse(JSON.stringify(_rasd_dataTableProperties));

    newDT1.responsive=false;

    newDT1.bSort=false;



    newDT1.destroy = true;

    $("#B20_TABLE").removeClass( "rasdTableN" );

    xTable1 = $("#B20_TABLE").DataTable(newDT1);



    $(''a.toggle-ser'').on(''click'', function (e) {

        e.preventDefault();

        $("#SHOWHIDESER_RASD").val(!xTable1.column(1).visible());

        // Toggle the visibility

        xTable1.column(1).visible(!xTable1.column(1).visible());

        xTable1.column(2).visible(!xTable1.column(2).visible());

        xTable1.column(3).visible(!xTable1.column(3).visible());

        xTable1.column(4).visible(!xTable1.column(4).visible());

        xTable1.column(5).visible(!xTable1.column(5).visible());

        xTable1.column(6).visible(!xTable1.column(6).visible());

        xTable1.column(7).visible(!xTable1.column(7).visible());

        xTable1.column(8).visible(!xTable1.column(8).visible());

        xTable1.column(9).visible(!xTable1.column(9).visible());

        xTable1.column(10).visible(!xTable1.column(10).visible());

        xTable1.column(11).visible(!xTable1.column(11).visible());

        xTable1.column(12).visible(!xTable1.column(12).visible());





	    if ($("#SHOWHIDESER_RASD").val()==''false'') {

	       $(''.rasdTxB20NAMEID input'').css({''width'': ''200px''});

	       $(''.rasdTxB20LABEL input'').css({''width'': ''200px''});

	       $(''.rasdTxB20ELEMENT select'').css({''width'': ''200px''});

	       $(''.rasdTxB20LINKID select'').css({''width'': ''200px''});

	       $(''.rasdTxB20FIELDID input'').css({''width'': ''200px''});

		} else {

	       $(''.rasdTxB20NAMEID input'').css({''width'': ''80px''});

	       $(''.rasdTxB20LABEL input'').css({''width'': ''80px''});

	       $(''.rasdTxB20ELEMENT select'').css({''width'': ''100px''});

	       $(''.rasdTxB20LINKID select'').css({''width'': ''100px''});

		   $(''.rasdTxB20FIELDID input'').css({''width'': ''80px''});

		}



    });



      $(''a.toggle-ui'').on(''click'', function (e) {

        e.preventDefault();

        $("#SHOWHIDEUI_RASD").val(!xTable1.column(13).visible());

        // Toggle the visibility

        xTable1.column(13).visible(!xTable1.column(13).visible());

        xTable1.column(14).visible(!xTable1.column(14).visible());

        xTable1.column(15).visible(';
 v_vc(14) := '!xTable1.column(15).visible());

        xTable1.column(16).visible(!xTable1.column(16).visible());

        xTable1.column(17).visible(!xTable1.column(17).visible());

        xTable1.column(18).visible(!xTable1.column(18).visible());

        xTable1.column(19).visible(!xTable1.column(19).visible());

        xTable1.column(20).visible(!xTable1.column(20).visible());

        xTable1.column(21).visible(!xTable1.column(21).visible());



	    if ($("#SHOWHIDEUI_RASD").val()==''false'') {

	       $(''.rasdTxB20ORDERBY input'').css({''width'': ''150px''});

	       $(''.rasdTxB20FORMAT input'').css({''width'': ''200px''});

	       $(''.rasdTxB20DEFAULTVAL input'').css({''width'': ''300px''});

		   $(''.rasdTxB20FIELDID input'').css({''width'': ''200px''});

		} else {

	       $(''.rasdTxB20ORDERBY input'').css({''width'': ''80px''});

	       $(''.rasdTxB20FORMAT input'').css({''width'': ''80px''});

	       $(''.rasdTxB20DEFAULTVAL input'').css({''width'': ''80px''});

		   $(''.rasdTxB20FIELDID input'').css({''width'': ''80px''});

		}

    });



  if ($("#SHOWHIDESER_RASD").val() == ''false'') {

        xTable1.column(1).visible(false);

        xTable1.column(2).visible(false);

        xTable1.column(3).visible(false);

        xTable1.column(4).visible(false);

        xTable1.column(5).visible(false);

        xTable1.column(6).visible(false);

        xTable1.column(7).visible(false);

        xTable1.column(8).visible(false);

        xTable1.column(9).visible(false);

        xTable1.column(10).visible(false);

        xTable1.column(11).visible(false);

        xTable1.column(12).visible(false);

  }



  if ($("#SHOWHIDEUI_RASD").val() == ''false'') {

        xTable1.column(13).visible(false);

        xTable1.column(14).visible(false);

        xTable1.column(15).visible(false);

        xTable1.column(16).visible(false);

        xTable1.column(17).visible(false);

        xTable1.column(18).visible(false);

        xTable1.column(19).visible(false);

        xTable1.column(20).visible(false);

        xTable1.column(21).visible(false);

  }





// aded icon wher LOV is

ShowLOVIcon();



 });







$(function() {





  rasd_codemirror_B10SQLTEXT_X_RASD();





});


]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>FORM_JS_R';
 v_vc(15) := 'EF(82)</triggerid><plsql><![CDATA[$(function() {



  addSpinner();



});



$(function() {



  $(".rasdFormMenu").html("<%= RASDC_LIBRARY.showMeni(THIS_FORM, PFORMID, null, lang) %>");





  $(document).ready(function () {

   $(".dialog").dialog({ autoOpen: false });

  });





});
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>FORM_UIHEAD</triggerid><plsql><![CDATA[<script type="text/javascript" src="RASDC2_FIELDSONBLOCK.codemirrorjs?n=B10SQLTEXT_X_RASD&h=150&d=<%= PSESSSTORAGEENABLED(1) %>"></script>
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>GBUTTONPREV</blockid><triggerid>ON_UI</triggerid><plsql><![CDATA[htp.p( ''<input type=button class="rasdButton" value="'' ||

                   RASDI_TRNSLT.text(''Preview'', lang) || ''" '' ||

                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,

                                ''disabled="disabled" style="background-color: red;" title="'' ||RASDI_TRNSLT.text(''Program has ERRORS!'',lang)||''" '',

                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,

                                     ''style="background-color: orange;" title="'' ||RASDI_TRNSLT.text(''Programa has changes. Compile it.'',lang) ||

                                     ''" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

									 ,

                                     ''style="background-color: green;" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

                                )

			       ) || ''>''

);
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid>GBUTTONPREV</rblockid></trigger><trigger><blockid>HINTCONTENT</blockid><triggerid>ON_UI</triggerid><plsql><![CDATA[htp.p(''<div style="display: none;">'');

htp.p(rasdc_hints.getHint(replace(this_form,''2'','''')||''_DIALOG'',lang));

htp.p(''</div>'');
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid>HINTCONTENT</rblockid></trigger><tr';
 v_vc(16) := 'igger><blockid></blockid><triggerid>POST_ACTION</triggerid><plsql><![CDATA[if action = GBUTTONCOMPILE then



if PRFORMID is not null then

message := RASDI_TRNSLT.text(''You can multi compile only templete form.'',lang);

else

if COMPILEALL is null then



message := RASDI_TRNSLT.text(''Do you want to compile all forms referenced to this form? Check <input name="COMPILEALL" value="Y" type="checkbox"/> and Compile again.'',lang);



else



if COMPILEALL = ''Y'' then



message := RASDI_TRNSLT.text(''Compiled forms: '',lang);



declare

  sp varchar2(1000);

begin

for r in (select f.form , f.label, f.formid

from rasd_forms f

where (formid) in (

select x.formid

from rasd_forms f, rasd_references x

where f.formid = pformid

  and f.lobid = x.rlobid

  and f.form = x.rform

)

order by 1 ) loop



message := message ||''<br/> ''||r.form||'' :  '';



compile(r.formid , r.form , lang ,  sp , ''''  , compid);

message := message || sp;



end loop;



end;



end if;



end if;

end if;

pselect;

poutput;



end if;
]]></plsql><plsqlspec><![CDATA[  -- The execution after default execution based on  ACTION.
  -- Delete this code when you have new actions and add your own.
  if  nvl(ACTION,GBUTTONSRC) not in (  GBUTTONBCK, GBUTTONFWD, GBUTTONSRC, GBUTTONSAVE, GBUTTONCLR ) then
    raise_application_error(''-20000'', ''ACTION="''||ACTION||''" is not defined. Define it in POST_ACTION trigger.'');
  end if;

]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>POST_COMMIT</triggerid><plsql><![CDATA[declare

     n number;

    begin

     select count(*) into n from rasd_references where formid = pformid;



     if n > 0 and prformid is null then



       message := RASDI_TRNSLT.text(''You would like to delete referenced form. Would you like to delete referenced data or unlink referenced data! '', lang);



       message :=  message ||  RASDI_TRNSLT.text(''Delete'', lang)||''<input type="radio" name="UNLINKDELETE" value="DEL"/> / '';

       message :=  message ||  RASDI_TRNSLT.text(''Unlink'', lang)||''<input type="radio" name="UNLINKDELETE" value="UNL"/> / '';

       message :=  message ||  RASDI_TRNSLT.text(''No change'', lang)||''<input type="radio" name="UNLINKDELETE" value=""/>.'';



--rlog(''FFF"''||UNLINKDELETE||''"'');

       if UNLINKDELETE is not null then

--rlog(''ZZ''||UNLINK';
 v_vc(17) := 'DELETE);

--rlog( to_char(instr(UNLINKDELETE,''DEL''))  );

         if  UNLINKDELETE = ''DEL'' then

--rlog(''ZZ3''||UNLINKDELETE);



             rasdc_library.deleteFormRefData(pformid);

             message := RASDI_TRNSLT.text(''Referenced data is deleted.'',lang);

         elsif UNLINKDELETE = ''UNL'' then

--rlog(''ZZ4''||UNLINKDELETE);

             rasdc_library.unlinkFormRefData(pformid);

             message := RASDI_TRNSLT.text(''Referenced data is unlinked.'',lang);

         end if;

           delete from rasd_references where formid = pformid;

           update rasd_forms f set f.referenceyn = ''N'' where f.formid = pformid;



       end if;



     else

--rlog(''YY4''||UNLINKDELETE);

      RASDC_LIBRARY.deleteformrefdata(pformid);

      delete from rasd_references where formid = pformid;



      for r in (select * from rasd_forms where formid = prformid) loop

        insert into rasd_references (formid, rlobid, rform) values (pformid, r.lobid, r.form);

        update rasd_forms f set f.referenceyn = ''Y'' where f.formid = pformid;

        RASDC_LIBRARY.copyformrefdata( r.formid , pformid);

      end loop;



     end if;





     commit;

      null;

    end;









update RASD_FORMS set change = sysdate where formid = PFORMID;




]]></plsql><plsqlspec><![CDATA[-- Triggers after executing DML operations on DB blocks.
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>POST_SUBMIT</triggerid><plsql><![CDATA[----put procedure in the begining of trigger;

post_submit_template;





GBUTTONCOMPILE := RASDI_TRNSLT.text(''Multi compile'',lang);
]]></plsql><plsqlspec><![CDATA[-- Executing after filling fields on submit.
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>_ changes</triggerid><plsql><![CDATA[  function changes(p_log out varchar2) return varchar2 is

  begin



    p_log := ''/* Change LOG:

20230330 - Created new 2 version

20160627 - Included reference form future.

*/'';

    return version;

 end;
]]></plsql><plsqlspec><![CDATA[  function changes(p_log out varchar2) return varchar2;
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><';
 v_vc(18) := 'rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>procedure custom template</triggerid><plsql><![CDATA[function showLabel(plabel varchar2, pcolor varchar2 default ''U'', pshowdialog number default 0)

return varchar2 is

   v__ varchar2(32000);

begin



v__ := RASDI_TRNSLT.text(plabel, lang);



if pshowdialog = 1 then

v__ := v__ || rasdc_hints.linkDialog(replace(replace(plabel,'' '',''''),''.'',''''),lang,replace(this_form,''2'','''')||''_DIALOG'');

end if;



if pcolor is null then



return v__;



else



return ''<font color="''||pcolor||''">''||v__||''</font>'';



end if;





end;
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>procedure post_submit template</triggerid><plsql><![CDATA[procedure post_submit_template is

begin

RASDC_LIBRARY.checkprivileges(PFORMID);

begin

      select upper(form)

        into pform

        from RASD_FORMS

       where formid = PFORMID;

exception when others then null;

end;



if ACTION = GBUTTONSAVE or ACTION = GBUTTONCOMPILE then

  if rasdc_library.allowEditing(pformid) then

     null;

  else

     ACTION := GBUTTONSRC;

	 message := message || RASDI_TRNSLT.text(''User has no privileges to save data!'', lang);

  end if;

end if;





if rasdc_library.allowEditing(pformid) then

   GBUTTONSAVE#SET.visible := true;

   GBUTTONCOMPILE#SET.visible := true;

else

   GBUTTONSAVE#SET.visible := false;

   GBUTTONCOMPILE#SET.visible := false;

end if;





VUSER := rasdi_client.secGetUsername;

VLOB := rasdi_client.secGetLOB;

if lang is null then lang := rasdi_client.c_defaultLanguage; end if;



end;
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>procedure_compile_template</triggerid><plsql><![CDATA[procedure compile(pformid number, pform varchar2, lang varchar2,  sporocilo in out varchar2, refform varchar2 default ''''  , pcompid in out number) is

      v_server RASD_ENGINES.server%type;

      cid      pls_integer;

      n        pls_integer;

      vup      varchar2(30) := rasdi_client.secGetUsername;

begin

        rasdc_library.log(this_form,pformid, ''COMPILE_S'', pcomp';
 v_vc(19) := 'id);



        begin

          if instr( upper(rasd_client.secGet_PATH_INFO), upper(pform)) > 0

			 then

            sporocilo := RASDI_TRNSLT.text(''From is not generated.'', lang);

          else



            select server

              into v_server

              from RASD_FORMS_COMPILED fg, RASD_ENGINES g

             where fg.engineid = g.engineid

               and fg.formid = PFORMID

               and fg.editor = vup

               and (fg.lobid = rasdi_client.secGetLOB or

                   fg.lobid is null and rasdi_client.secGetLOB is null);



            cid     := dbms_sql.open_cursor;



            dbms_sql.parse(cid,

                           ''begin '' || v_server || ''.c_debug := false;''|| v_server || ''.form('' || PFORMID ||

                           '','''''' || lang || '''''');end;'',

                           dbms_sql.native);



            n       := dbms_sql.execute(cid);



            dbms_sql.close_cursor(cid);



            sporocilo := RASDI_TRNSLT.text(''From is generated.'', lang) || rasdc_library.checknumberofsubfields(PFORMID);



        if refform is not null then

           sporocilo :=  sporocilo || ''<br/> - ''||  RASDI_TRNSLT.text(''To unlink referenced code check:'', lang)||''<input type="checkbox" name="UNLINK" value="Y"/>.'';

        end if;



          end if;

        exception

          when others then

            if sqlcode = -24344 then



            sporocilo := RASDI_TRNSLT.text(''Form is generated with compilation error. Check your code.'', lang)||''(''||sqlerrm||'')'';



            else

            sporocilo := RASDI_TRNSLT.text(''Form is NOT generated - internal RASD error.'', lang) || ''(''||sqlerrm||'')<br>''||

                         RASDI_TRNSLT.text(''To debug run: '', lang) || ''begin '' || v_server || ''.form('' || PFORMID ||

                         '','''''' || lang || '''''');end;'' ;

            end if;

        end;

        rasdc_library.log(this_form,pformid, ''COMPILE_E'', pcompid);

end;
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid></trigger></triggers><elements><element><elementid>1</elementid><pelementid>0</pelementid><orderby>1</orderby><el';
 v_vc(20) := 'ement>HTML_</element><type></type><id>HTML</id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</html]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<html]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>2</elementid><pelementid>1</pelementid><orderby>1</orderby><element>HEAD_</element><type></type><id>HEAD</id><nameid>HEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</head]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<head]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%
htpClob(rasd_client.getHtmlJSLibrary(''HEAD'',''<%= rasdc_library.formName(PFORMID, LANG) %>''));
htp.p('''');
htp.p(''<script type="text/javascript">'');
formgen_js;
htp.p(''</script>'');
htpClob(FORM_UIHEAD);
htp.p(''<style type="text/css">'');
htpClo';
 v_vc(21) := 'b(FORM_CSS);
htp.p(''</style><script type="text/javascript">''); htpClob(FORM_JS); htp.p(''</script>'');
%>]]></value><valuecode><![CDATA['');
htpClob(rasd_client.getHtmlJSLibrary(''HEAD'',''''|| rasdc_library.formName(PFORMID, LANG) ||''''));
htp.p('''');
htp.p(''<script type="text/javascript">'');
formgen_js;
htp.p(''</script>'');
htpClob(FORM_UIHEAD);
htp.p(''<style type="text/css">'');
htpClob(FORM_CSS);
htp.p(''</style><script type="text/javascript">''); htpClob(FORM_JS); htp.p(''</script>'');

htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>15</elementid><pelementid>1</pelementid><orderby>1</orderby><element>BODY_</element><type></type><id>BODY</id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</body]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<body]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>16</elementid><pelementid>15</pelementid><orderby>3</orderby><element>FORM_</element><type>F</type><id>RASDC2_REFERENCES</id><nameid>RASDC2_REFERENCES</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>A_ACTION</attribute><type>A</type><text></text><name><![CDATA[action]]></name><value><![CDATA[?]]></value><valuecode><![CDATA[="?"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N';
 v_vc(22) := '</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_METHOD</attribute><type>A</type><text></text><name><![CDATA[method]]></name><value><![CDATA[post]]></value><valuecode><![CDATA[="post"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[RASDC2_REFERENCES]]></value><valuecode><![CDATA[="RASDC2_REFERENCES"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</form]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<form]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>17</elementid><pelementid>15</pelementid><orderby>1</orderby><element>FORM_LAB</element><type>F</type><id>RASDC2_REFERENCES_LAB</id><nameid>RASDC2_REFERENCES_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormLab]]></value><valuecode><![CDATA[="rasdFormLab"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></';
 v_vc(23) := 'rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_REFERENCES_LAB]]></value><valuecode><![CDATA[="RASDC2_REFERENCES_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= rasd_client.getHtmlHeaderDataTable(''RASDC2_REFERENCES_LAB'',''<%= rasdc_library.formName(PFORMID, LANG) %>'') %>     ]]></value><valuecode><![CDATA[''|| rasd_client.getHtmlHeaderDataTable(''RASDC2_REFERENCES_LAB'',''''|| rasdc_library.formName(PFORMID, LANG) ||'''') ||''     ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>18</elementid><pelementid>15</pelementid><orderby>2</orderby><element>FORM_MENU</element><type>F</type><id>RASDC2_REFERENCES_MENU</id><nameid>RASDC2_REFERENCES_MENU</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMenu]]></value><valuecode><![CDATA[="rasdFormMenu"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hi';
 v_vc(24) := 'ddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_REFERENCES_MENU]]></value><valuecode><![CDATA[="RASDC2_REFERENCES_MENU"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= rasd_client.getHtmlMenuList(''RASDC2_REFERENCES_MENU'') %>     ]]></value><valuecode><![CDATA[''|| rasd_client.getHtmlMenuList(''RASDC2_REFERENCES_MENU'') ||''     ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>19</elementid><pelementid>15</pelementid><orderby>9999</orderby><element>FORM_BOTTOM</element><type>F</type><id>RASDC2_REFERENCES_BOTTOM</id><nameid>RASDC2_REFERENCES_BOTTOM</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormBottom]]></value><valuecode><![CDATA[="rasdFormBottom"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hidd';
 v_vc(25) := 'enyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_REFERENCES_BOTTOM]]></value><valuecode><![CDATA[="RASDC2_REFERENCES_BOTTOM"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= rasd_client.getHtmlFooter(version , substr(''RASDC2_REFERENCES_BOTTOM'',1,instr(''RASDC2_REFERENCES_BOTTOM'', ''_'',-1)-1) , '''') %>]]></value><valuecode><![CDATA[''|| rasd_client.getHtmlFooter(version , substr(''RASDC2_REFERENCES_BOTTOM'',1,instr(''RASDC2_REFERENCES_BOTTOM'', ''_'',-1)-1) , '''') ||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>20</elementid><pelementid>16</pelementid><orderby>3</orderby><element>FORM_DIV</element><type>F</type><id>RASDC2_REFERENCES_DIV</id><nameid>RASDC2_REFERENCES_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdForm]]></value><valuecode><';
 v_vc(26) := '![CDATA[="rasdForm"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_REFERENCES_DIV]]></value><valuecode><![CDATA[="RASDC2_REFERENCES_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>21</elementid><pelementid>20</pelementid><orderby>1</orderby><element>FORM_HEAD</element><type>F</type><id>RASDC2_REFERENCES_HEAD</id><nameid>RASDC2_REFERENCES_HEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormHead]]></value><valuecode><![CDATA[="rasdFormHead"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_REFERENCES_HEAD]]></value><valuecode><![CDATA[="RASDC2_REFERENCES_HEAD"]]></valuecode><forloop></forloop><endloop></endloop><source></sou';
 v_vc(27) := 'rce><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>22</elementid><pelementid>20</pelementid><orderby>5</orderby><element>FORM_BODY</element><type>F</type><id>RASDC2_REFERENCES_BODY</id><nameid>RASDC2_REFERENCES_BODY</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormBody]]></value><valuecode><![CDATA[="rasdFormBody"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_REFERENCES_BODY]]></value><valuecode><![CDATA[="RASDC2_REFERENCES_BODY"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid><';
 v_vc(28) := '/attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>23</elementid><pelementid>20</pelementid><orderby>9999</orderby><element>FORM_FOOTER</element><type>F</type><id>RASDC2_REFERENCES_FOOTER</id><nameid>RASDC2_REFERENCES_FOOTER</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormFooter]]></value><valuecode><![CDATA[="rasdFormFooter"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_REFERENCES_FOOTER]]></value><valuecode><![CDATA[="RASDC2_REFERENCES_FOOTER"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>24</elementid><pelementid>20</pelementid><orderby>2</orde';
 v_vc(29) := 'rby><element>FORM_RESPONSE</element><type>F</type><id>RASDC2_REFERENCES_RESPONSE</id><nameid>RASDC2_REFERENCES_RESPONSE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormResponse]]></value><valuecode><![CDATA[="rasdFormResponse"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_REFERENCES_RESPONSE]]></value><valuecode><![CDATA[="RASDC2_REFERENCES_RESPONSE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>25</elementid><pelementid>24</pelementid><orderby>9998</orderby><element>FORM_MESSAGE</element><type>F</type><id>RASDC2_REFERENCES_MESSAGE</id><nameid>RASDC2_REFERENCES_MESSAGE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMessage]]></value><valuecode><![CDATA[="r';
 v_vc(30) := 'asdFormMessage"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_REFERENCES_MESSAGE]]></value><valuecode><![CDATA[="RASDC2_REFERENCES_MESSAGE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>26</elementid><pelementid>24</pelementid><orderby>9996</orderby><element>FORM_ERROR</element><type>F</type><id>RASDC2_REFERENCES_ERROR</id><nameid>RASDC2_REFERENCES_ERROR</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMessage error]]></value><valuecode><![CDATA[="rasdFormMessage error"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_REFERENCES_ERROR]]></value><valuecode><![CDATA[="RASDC2_REFERENCES_ERROR"]]></valuecode><forloop></forloop><e';
 v_vc(31) := 'ndloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>27</elementid><pelementid>24</pelementid><orderby>9997</orderby><element>FORM_WARNING</element><type>F</type><id>RASDC2_REFERENCES_WARNING</id><nameid>RASDC2_REFERENCES_WARNING</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMessage warning]]></value><valuecode><![CDATA[="rasdFormMessage warning"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_REFERENCES_WARNING]]></value><valuecode><![CDATA[="RASDC2_REFERENCES_WARNING"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></te';
 v_vc(32) := 'xtid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>28</elementid><pelementid>22</pelementid><orderby>105</orderby><element>BLOCK_DIV</element><type>B</type><id>P_DIV</id><nameid>P_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdblock]]></value><valuecode><![CDATA[="rasdblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_DIV]]></value><valuecode><![CDATA[="P_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[><div>]]></value><valuecode><![CDATA[><div>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</div></div><% end if; %>]]></value><valuecode><![CDATA[</div></div>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></tex';
 v_vc(33) := 't><name></name><value><![CDATA[<% if  ShowBlockP_DIV  then %><div ]]></value><valuecode><![CDATA['');  if  ShowBlockP_DIV  then
htp.prn(''<div ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>29</elementid><pelementid>28</pelementid><orderby>1</orderby><element>CAPTION_</element><type>L</type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>30</elementid><pelementid>29</pelementid><orderby>1</orderby><element>FONT_</element><type>B</type><id>P_LAB</id><nameid>P_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[labelblock]]></value><valuecode><![CDATA[="labelblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid><';
 v_vc(34) := '/textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_LAB]]></value><valuecode><![CDATA[="P_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>31</elementid><pelementid>28</pelementid><orderby>106</orderby><element>TABLE_</element><type></type><id>P_TABLE</id><nameid>P_TABLE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_BORDER</attribute><type>A</type><text></text><name><![CDATA[border]]></name><value><![CDATA[0]]></value><valuecode><![CDATA[="0"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_TABLE_RASD]]></value><valuecode><![CDATA[=';
 v_vc(35) := '"P_TABLE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>32</elementid><pelementid>31</pelementid><orderby>3</orderby><element>TR_</element><type>B</type><id>P_BLOCK</id><nameid>P_BLOCK</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_BLOCK_NAME]]></value><valuecode><![CDATA[="P_BLOCK_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform';
 v_vc(36) := '><rid></rid></attribute></attributes></element><element><elementid>33</elementid><pelementid>22</pelementid><orderby>115</orderby><element>BLOCK_DIV</element><type>B</type><id>B10_DIV</id><nameid>B10_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdblock]]></value><valuecode><![CDATA[="rasdblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_DIV]]></value><valuecode><![CDATA[="B10_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[><div>]]></value><valuecode><![CDATA[><div>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</div></div><% end if; %>]]></value><valuecode><![CDATA[</div></div>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if  ShowBlockB10_DIV  then %><div ]]></value><valuecode><![CDATA['');  if  ShowBlockB10_DIV  then
htp.prn(''<div ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>34</elementid><pelem';
 v_vc(37) := 'entid>33</pelementid><orderby>1</orderby><element>CAPTION_</element><type>L</type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>35</elementid><pelementid>34</pelementid><orderby>1</orderby><element>FONT_</element><type>B</type><id>B10_LAB</id><nameid>B10_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[labelblock]]></value><valuecode><![CDATA[="labelblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_LAB]]></value><valuecode><![CDATA[="B10_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textco';
 v_vc(38) := 'de><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= RASDI_TRNSLT.text(''Forms referenced to the same reference:'',lang)%>]]></value><valuecode><![CDATA[''|| RASDI_TRNSLT.text(''Forms referenced to the same reference:'',lang)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>36</elementid><pelementid>33</pelementid><orderby>116</orderby><element>TABLE_N</element><type></type><id>B10_TABLE</id><nameid>B10_TABLE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_BORDER</attribute><type>A</type><text></text><name><![CDATA[border]]></name><value><![CDATA[1]]></value><valuecode><![CDATA[="1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTableN display]]></value><valuecode><![CDATA[="rasdTableN display"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attr';
 v_vc(39) := 'ibute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_TABLE_RASD]]></value><valuecode><![CDATA[="B10_TABLE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>37</elementid><pelementid>36</pelementid><orderby>3</orderby><element>TR_</element><type>B</type><id>B10_BLOCK</id><nameid>B10_BLOCK</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_BLOCK_NAME]]></value><valuecode><![CDATA[="B10_BLOCK_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop><![CDATA[''); end loop;
htp.prn('']]></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]>';
 v_vc(40) := '</value><valuecode><![CDATA[>]]></valuecode><forloop><![CDATA[''); for iB10 in 1..B10FORM.count loop
htp.prn('']]></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>38</elementid><pelementid>36</pelementid><orderby>2</orderby><element>THEAD_</element><type></type><id>B10_THEAD</id><nameid>B10_THEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>1</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>39</elementid><pelementid>38</pelementid><orderby>2</orderby><element>TR_</element><type></type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></';
 v_vc(41) := 'rid></attribute></attributes></element><element><elementid>40</elementid><pelementid>39</pelementid><orderby>1</orderby><element>TX_</element><type>E</type><id></id><nameid>B10FORM_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10FORM]]></value><valuecode><![CDATA[="rasdTxLabB10FORM"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>41</elementid><pelementid>40</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>B10FORM_LAB</id><nameid>B10FORM_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><va';
 v_vc(42) := 'luecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10FORM_LAB]]></value><valuecode><![CDATA[="B10FORM_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= showLabel(''Form'','''',0) %>]]></value><valuecode><![CDATA[''|| showLabel(''Form'','''',0) ||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>42</elementid><pelementid>39</pelementid><orderby>2</orderby><element>TX_</element><type>E</type><id></id><nameid>B10LABEL_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hidd';
 v_vc(43) := 'enyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10LABEL]]></value><valuecode><![CDATA[="rasdTxLabB10LABEL"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>43</elementid><pelementid>42</pelementid><orderby>2</orderby><element>FONT_</element><type>L</type><id>B10LABEL_LAB</id><nameid>B10LABEL_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10LABEL_LAB]]></value><valuecode><![CDATA[="B10LABEL_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attr';
 v_vc(44) := 'ibute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= showLabel(''Name'','''',0) %>]]></value><valuecode><![CDATA[''|| showLabel(''Name'','''',0) ||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>44</elementid><pelementid>22</pelementid><orderby>125</orderby><element>BLOCK_DIV</element><type>B</type><id>B20_DIV</id><nameid>B20_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdblock]]></value><valuecode><![CDATA[="rasdblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20_DIV]]></value><valuecode><![CDATA[="B20_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[><div>]]></value><valuecode><![CDATA[><div>]]></valuecode><forloop></fo';
 v_vc(45) := 'rloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</div></div><% end if; %>]]></value><valuecode><![CDATA[</div></div>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if  ShowBlockB20_DIV  then %><div ]]></value><valuecode><![CDATA['');  if  ShowBlockB20_DIV  then
htp.prn(''<div ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>45</elementid><pelementid>44</pelementid><orderby>1</orderby><element>CAPTION_</element><type>L</type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></';
 v_vc(46) := 'rform><rid></rid></attribute></attributes></element><element><elementid>46</elementid><pelementid>45</pelementid><orderby>1</orderby><element>FONT_</element><type>B</type><id>B20_LAB</id><nameid>B20_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[labelblock]]></value><valuecode><![CDATA[="labelblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20_LAB]]></value><valuecode><![CDATA[="B20_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= RASDI_TRNSLT.text(''Forms referenced to this form:'',lang)%>]]></value><valuecode><![CDATA[''|| RASDI_TRNSLT.text(''Forms referenced to this form:'',lang)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>47</elementid><pe';
 v_vc(47) := 'lementid>44</pelementid><orderby>126</orderby><element>TABLE_N</element><type></type><id>B20_TABLE</id><nameid>B20_TABLE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_BORDER</attribute><type>A</type><text></text><name><![CDATA[border]]></name><value><![CDATA[1]]></value><valuecode><![CDATA[="1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTableN display]]></value><valuecode><![CDATA[="rasdTableN display"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20_TABLE_RASD]]></value><valuecode><![CDATA[="B20_TABLE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>48</elementid><pelementid>47</pelementid><orderby>3</orderby><element>TR_</element><type>B</type><id>B20_BLOCK</id><nameid>B20_BLOCK</nameid><endtagelementid>0</endta';
 v_vc(48) := 'gelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20_BLOCK_NAME]]></value><valuecode><![CDATA[="B20_BLOCK_''||iB20||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop><![CDATA[''); end loop;
htp.prn('']]></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop><![CDATA[''); for iB20 in 1..B20FORM.count loop
htp.prn('']]></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>49</elementid><pelementid>47</pelementid><orderby>2</orderby><element>THEAD_</element><type></type><id>B20_THEAD</id><nameid>B20_THEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>1</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></';
 v_vc(49) := 'rform><rid></rid></attribute></attributes></element><element><elementid>50</elementid><pelementid>49</pelementid><orderby>2</orderby><element>TR_</element><type></type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>51</elementid><pelementid>50</pelementid><orderby>1</orderby><element>TX_</element><type>E</type><id></id><nameid>B20FORM_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB20]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB20"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB20FORM]]></value><valuecode><![CDATA[="rasdTxLabB20FORM"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]><';
 v_vc(50) := '/valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>52</elementid><pelementid>51</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>B20FORM_LAB</id><nameid>B20FORM_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20FORM_LAB]]></value><valuecode><![CDATA[="B20FORM_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid><';
 v_vc(51) := '/attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= showLabel(''Form'','''',0) %>]]></value><valuecode><![CDATA[''|| showLabel(''Form'','''',0) ||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>53</elementid><pelementid>50</pelementid><orderby>2</orderby><element>TX_</element><type>E</type><id></id><nameid>B20LABEL_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB20]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB20"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB20LABEL]]></value><valuecode><![CDATA[="rasdTxLabB20LABEL"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>54</elementid><pelementid>53</pelementid><orderby>';
 v_vc(52) := '2</orderby><element>FONT_</element><type>L</type><id>B20LABEL_LAB</id><nameid>B20LABEL_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20LABEL_LAB]]></value><valuecode><![CDATA[="B20LABEL_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= showLabel(''Name'','''',0) %>]]></value><valuecode><![CDATA[''|| showLabel(''Name'','''',0) ||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>55</elementid><pelementid>21</pelementid><orderby>-9</orderby><element>INPUT_HIDDEN</element><type>D</type><id>RECNUMB10</id><nameid>RECNUMB10</nameid><endtagelementid></endtagelementid><source';
 v_vc(53) := '></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RECNUMB10_NAME_RASD]]></value><valuecode><![CDATA[="RECNUMB10_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[RECNUMB10_NAME]]></value><valuecode><![CDATA[="RECNUMB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>';
 v_vc(54) := 'A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[RECNUMB10_VALUE]]></value><valuecode><![CDATA[="''||ltrim(to_char(RECNUMB10))||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>56</elementid><pelementid>21</pelementid><orderby>-1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>RECNUMB20</id><nameid>RECNUMB20</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></';
 v_vc(55) := 'valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RECNUMB20_NAME_RASD]]></value><valuecode><![CDATA[="RECNUMB20_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[RECNUMB20_NAME]]></value><valuecode><![CDATA[="RECNUMB20"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[RECNUMB20_VALUE]]></value><valuecode><![CDATA[="''||ltrim(to_char(RECNUMB20))||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><at';
 v_vc(56) := 'tribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>57</elementid><pelementid>21</pelementid><orderby>-1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>RECNUMP</id><nameid>RECNUMP</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RECNUMP_NAME_RASD]]></value><valuecode><![CDATA[="RECNUMP_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[RECNUMP_NAME]]></value><valuecode><![CDATA[="RECNUMP"]]></valuecode><forloop></forloop><endloop></endloop><source';
 v_vc(57) := '></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[RECNUMP_VALUE]]></value><valuecode><![CDATA[="''||ltrim(to_char(RECNUMP))||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>58</elementid><pelem';
 v_vc(58) := 'entid>21</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>ACTION</id><nameid>ACTION</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[ACTION_NAME_RASD]]></value><valuecode><![CDATA[="ACTION_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[ACTION_NAME]]></value><valuecode><![CDATA[="ACTION"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><text';
 v_vc(59) := 'id></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[ACTION_VALUE]]></value><valuecode><![CDATA[="''||ACTION||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>59</elementid><pelementid>21</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>PAGE</id><nameid>PAGE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><valu';
 v_vc(60) := 'e></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PAGE_NAME_RASD]]></value><valuecode><![CDATA[="PAGE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PAGE_NAME]]></value><valuecode><![CDATA[="PAGE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[PAGE_VALUE]]></value><valuecode><![CDATA[="''||ltrim(to_char(PAGE))||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><text';
 v_vc(61) := 'id></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>60</elementid><pelementid>21</pelementid><orderby>6</orderby><element>INPUT_HIDDEN</element><type>D</type><id>LANG</id><nameid>LANG</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[LANG_NAME_RASD]]></value><valuecode><![CDATA[="LANG_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[LANG_NAME]]></value><valuecode><![C';
 v_vc(62) := 'DATA[="LANG"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[LANG_VALUE]]></value><valuecode><![CDATA[="''||LANG||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes>';
 v_vc(63) := '</element><element><elementid>61</elementid><pelementid>21</pelementid><orderby>7</orderby><element>INPUT_HIDDEN</element><type>D</type><id>PFORMID</id><nameid>PFORMID</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PFORMID_NAME_RASD]]></value><valuecode><![CDATA[="PFORMID_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PFORMID_NAME]]></value><valuecode><![CDATA[="PFORMID"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source';
 v_vc(64) := '></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[PFORMID_VALUE]]></value><valuecode><![CDATA[="''||PFORMID||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>62</elementid><pelementid>21</pelementid><orderby>13</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONSAVE</id><nameid>GBUTTONSAVE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONSAVE#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONSAVE#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONSAVE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.';
 v_vc(65) := 'custom),''CLASS="'')+7 , instr(upper(GBUTTONSAVE#SET.custom),''"'',instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONSAVE#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONSAVE#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONSAVE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONSAVE#SET.custom),''"'',instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONSAVE_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONSAVE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONSAVE_NAME]]></value><valuecode><![CDATA[="GBUTTONSAVE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><va';
 v_vc(66) := 'lueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONSAVE_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONSAVE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SE';
 v_vc(67) := 'T.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONSAVE#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONSAVE#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.error is not null then htp.p('' title="''||GBUTTONSAVE#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.error is not null then htp.p('' title="''||GBUTTONSAVE#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.info is not null then htp.p('' title="''||GBUTTONSAVE#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.info is not null then htp.p('' title="''||GBUTTONSAVE#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</att';
 v_vc(68) := 'ribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONSAVE  then
htp.prn('''');  if GBUTTONSAVE#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONSAVE#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>63</elementid><pelementid>23</pelementid><orderby>10011</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONSAVE</id><nameid>GBUTTONSAVE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONSAVE#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONSAVE#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONSAVE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONSAVE#SET.custom),''"'',instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONSAVE#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONSAVE#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONSAVE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONSAVE#SET.custom),''"'',instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><val';
 v_vc(69) := 'ueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONSAVE_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONSAVE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONSAVE_NAME]]></value><valuecode><![CDATA[="GBUTTONSAVE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rfo';
 v_vc(70) := 'rm></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONSAVE_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONSAVE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</ord';
 v_vc(71) := 'erby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONSAVE#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONSAVE#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.error is not null then htp.p('' title="''||GBUTTONSAVE#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.error is not null then htp.p('' title="''||GBUTTONSAVE#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.info is not null then htp.p('' title="''||GBUTTONSAVE#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.info is not null then htp.p('' title="''||GBUTTONSAVE#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONSAVE  then
htp.prn('''');  if GBUTTONSAVE#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONSAVE#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attribut';
 v_vc(72) := 'es></element><element><elementid>64</elementid><pelementid>21</pelementid><orderby>14</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONCOMPILE</id><nameid>GBUTTONCOMPILE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONCOMPILE#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONCOMPILE#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONCOMPILE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),''"'',instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONCOMPILE#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONCOMPILE#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONCOMPILE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),''"'',instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONCOMPILE_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONCOMPILE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></va';
 v_vc(73) := 'lueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONCOMPILE_NAME]]></value><valuecode><![CDATA[="GBUTTONCOMPILE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONCOMPILE_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONCOMPILE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><texti';
 v_vc(74) := 'd></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONCOMPILE#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONCOMPILE#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.error is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.error is not null then ht';
 v_vc(75) := 'p.p('' title="''||GBUTTONCOMPILE#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.info is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.info is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONCOMPILE  then
htp.prn('''');  if GBUTTONCOMPILE#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>65</elementid><pelementid>23</pelementid><orderby>10012</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONCOMPILE</id><nameid>GBUTTONCOMPILE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONCOMPILE#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONCOMPILE#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONCOMPILE#';
 v_vc(76) := 'SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),''"'',instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONCOMPILE#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONCOMPILE#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONCOMPILE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),''"'',instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONCOMPILE_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONCOMPILE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONCOMPILE_NAME]]></value><valuecode><![CDATA[="GBUTTONCOMPILE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></val';
 v_vc(77) := 'ue><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONCOMPILE_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONCOMPILE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><tex';
 v_vc(78) := 't></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONCOMPILE#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONCOMPILE#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.error is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.error is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.info is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.info is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop><';
 v_vc(79) := '/endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONCOMPILE  then
htp.prn('''');  if GBUTTONCOMPILE#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>66</elementid><pelementid>21</pelementid><orderby>15</orderby><element>INPUT_RESET</element><type>D</type><id>GBUTTONRES</id><nameid>GBUTTONRES</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONRES#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONRES#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONRES#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONRES#SET.custom),''"'',instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONRES#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONRES#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONRES#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONRES#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONRES#SET.custom),''"'',instr(upper(GBUTTONR';
 v_vc(80) := 'ES#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONRES#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONRES_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONRES_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONRES_NAME]]></value><valuecode><![CDATA[="GBUTTONRES"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[reset]]></value><valuecode><![CDATA[="reset"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hidde';
 v_vc(81) := 'nyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONRES_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONRES||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textco';
 v_vc(82) := 'de><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONRES#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONRES#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.error is not null then htp.p('' title="''||GBUTTONRES#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.error is not null then htp.p('' title="''||GBUTTONRES#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.info is not null then htp.p('' title="''||GBUTTONRES#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.info is not null then htp.p('' title="''||GBUTTONRES#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONRES  then
htp.prn('''');  if GBUTTONRES#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONRES#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></t';
 v_vc(83) := 'extcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>67</elementid><pelementid>23</pelementid><orderby>10013</orderby><element>INPUT_RESET</element><type>D</type><id>GBUTTONRES</id><nameid>GBUTTONRES</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONRES#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONRES#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONRES#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONRES#SET.custom),''"'',instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONRES#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONRES#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONRES#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONRES#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONRES#SET.custom),''"'',instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONRES#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONRES_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONRES_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid';
 v_vc(84) := '><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONRES_NAME]]></value><valuecode><![CDATA[="GBUTTONRES"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[reset]]></value><valuecode><![CDATA[="reset"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONRES_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONRES||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby>';
 v_vc(85) := '<attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONRES#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONRES#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.error is not null then htp.p('' title="''||GBUTTONRES#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.error is not null then htp.p('' title="''||GBUTTONRES#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid>';
 v_vc(86) := '</valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.info is not null then htp.p('' title="''||GBUTTONRES#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.info is not null then htp.p('' title="''||GBUTTONRES#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONRES  then
htp.prn('''');  if GBUTTONRES#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONRES#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>68</elementid><pelementid>21</pelementid><orderby>16</orderby><element>PLSQL_</element><type>D</type><id>GBUTTONPREV</id><nameid>GBUTTONPREV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONPREV_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONPREV_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CD';
 v_vc(87) := 'ATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONPREV#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONPREV#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.error is not null then htp.p('' title="''||GBUTTONPREV#SET.error||''"''); e';
 v_vc(88) := 'nd if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.error is not null then htp.p('' title="''||GBUTTONPREV#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.info is not null then htp.p('' title="''||GBUTTONPREV#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.info is not null then htp.p('' title="''||GBUTTONPREV#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span>'');  end if;
htp.prn('']]></name><value><![CDATA[</span><% end if; %>]]></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONPREV  then
htp.prn('''');  if GBUTTONPREV#SET.visible then
htp.prn(''<span]]></name><value><![CDATA[<% if GBUTTONPREV#SET.visible then %><span]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% GBUTTONPREV_VALUE %>]]></value><valuecode><![CDATA['');  htp.p( ''<input type=button class="rasdButton" value="'' ||

                   RASDI_TRNSLT.text(''Preview'', lang) || ''" '' ||

                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,

                                ''disabled="disabled" style="background-color: red;" title="'' ||RASDI_TRNSLT.text(''Program has ERRORS!'',lang)||''" '',

                                owa_';
 v_vc(89) := 'util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,

                                     ''style="background-color: orange;" title="'' ||RASDI_TRNSLT.text(''Programa has changes. Compile it.'',lang) ||

                                     ''" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

									 ,

                                     ''style="background-color: green;" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

                                )

			       ) || ''>''

);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>69</elementid><pelementid>23</pelementid><orderby>10014</orderby><element>PLSQL_</element><type>D</type><id>GBUTTONPREV</id><nameid>GBUTTONPREV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONPREV_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONPREV_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</a';
 v_vc(90) := 'ttribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONPREV#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONPREV#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.error is not null then htp.p('' title="''||GBUTTONPREV#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.error is not null then htp.p('' title="''||GBUTTONPREV#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.info is not null then htp.p('' title="''||GBUTTONPREV#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.info is not null then htp.p('' title="''||GBUTTONPREV#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><sourc';
 v_vc(91) := 'e></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span>'');  end if;
htp.prn('']]></name><value><![CDATA[</span><% end if; %>]]></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONPREV  then
htp.prn('''');  if GBUTTONPREV#SET.visible then
htp.prn(''<span]]></name><value><![CDATA[<% if GBUTTONPREV#SET.visible then %><span]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% GBUTTONPREV_VALUE %>]]></value><valuecode><![CDATA['');  htp.p( ''<input type=button class="rasdButton" value="'' ||

                   RASDI_TRNSLT.text(''Preview'', lang) || ''" '' ||

                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,

                                ''disabled="disabled" style="background-color: red;" title="'' ||RASDI_TRNSLT.text(''Program has ERRORS!'',lang)||''" '',

                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,

                                     ''style="background-color: orange;" title="'' ||RASDI_TRNSLT.text(''Programa has changes. Compile it.'',lang) ||

                                     ''" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

									 ,

                                     ''style="background-color: green;" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

                                )

			       ) || ''>''

);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elemen';
 v_vc(92) := 'tid>70</elementid><pelementid>26</pelementid><orderby>10046</orderby><element>FONT_</element><type>D</type><id>ERROR</id><nameid>ERROR</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[ERROR_NAME_RASD]]></value><valuecode><![CDATA[="ERROR_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode>';
 v_vc(93) := '</textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[ERROR_VALUE]]></value><valuecode><![CDATA[''||ERROR||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>71</elementid><pelementid>25</pelementid><orderby>10049</orderby><element>FONT_</element><type>D</type><id>MESSAGE</id><nameid>MESSAGE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><fo';
 v_vc(94) := 'rloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[MESSAGE_NAME_RASD]]></value><valuecode><![CDATA[="MESSAGE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></tex';
 v_vc(95) := 't><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[MESSAGE_VALUE]]></value><valuecode><![CDATA[''||MESSAGE||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>72</elementid><pelementid>27</pelementid><orderby>10049</orderby><element>FONT_</element><type>D</type><id>WARNING</id><nameid>WARNING</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid>';
 v_vc(96) := '<rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[WARNING_NAME_RASD]]></value><valuecode><![CDATA[="WARNING_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></sourc';
 v_vc(97) := 'e><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[WARNING_VALUE]]></value><valuecode><![CDATA[''||WARNING||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>73</elementid><pelementid>21</pelementid><orderby>290876</orderby><element>PLSQL_</element><type>D</type><id>HINTCONTENT</id><nameid>HINTCONTENT</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[HINTCONTENT_NAME_RASD]]></value><valuecode><![CDATA[="HINTCONTENT_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldHINTCONTENT  then
htp.prn(''<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rf';
 v_vc(98) := 'orm></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% HINTCONTENT_VALUE %>]]></value><valuecode><![CDATA['');  htp.p(''<div style="display: none;">'');

htp.p(rasdc_hints.getHint(replace(this_form,''2'','''')||''_DIALOG'',lang));

htp.p(''</div>'');
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>74</elementid><pelementid>23</pelementid><orderby>300874</orderby><element>PLSQL_</element><type>D</type><id>HINTCONTENT</id><nameid>HINTCONTENT</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[HINTCONTENT_NAME_RASD]]></value><valuecode><![CDATA[="HINTCONTENT_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldHINTCONTENT  then
htp.prn(''<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% HINTCONTENT_VALUE %>]]></value><valuecode><![CDATA['');  htp.p(''<div style="display: none;">'');

htp.p(rasdc_hints.getHint(replace(this_form,''2'','''')||''_DIALOG'',lang));

htp.p(';
 v_vc(99) := '''</div>'');
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>75</elementid><pelementid>32</pelementid><orderby>1</orderby><element>HIDDFIELD_</element><type></type><id></id><nameid>P</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[hiddenRowItems]]></value><valuecode><![CDATA[="hiddenRowItems"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_ROW]]></value><valuecode><![CDATA[=""]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[P_ROW]]></value><valuecode><![CDATA[=""]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[1]]></value><valuecode><![CDATA[="1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobi';
 v_vc(100) := 'd><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>76</elementid><pelementid>75</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>SESSSTORAGEENABLED</id><nameid>SESSSTORAGEENABLED</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[SESSSTORAGEENABLED_NAME_RASD]]></value><valuecode><![CDATA[="SESSSTORAGEENABLED_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[SESSSTORAGEENABLED_NAME]]></value><valuecode><![CDATA[="SESSSTORAGEENABLED_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A<';
 v_vc(101) := '/type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[SESSSTORAGEENABLED_VALUE]]></value><valuecode><![CDATA[="''||PSESSSTORAGEENABLED(1)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>77</elementid><pelementid>32</pelementid><orderby>-19</orderby><element>TX_</element><type>E</type><id></id><nameid>PRFORMID_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></r';
 v_vc(102) := 'lobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockP]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockP"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabPRFORMID]]></value><valuecode><![CDATA[="rasdTxLabPRFORMID"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>78</elementid><pelementid>77</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>PRFORMID_LAB</id><nameid>PRFORMID_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</att';
 v_vc(103) := 'ribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PRFORMID_LAB]]></value><valuecode><![CDATA[="PRFORMID_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</span><% end if; %>]]></value><valuecode><![CDATA[</span>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if PRFORMID#SET(1).visible then %><span]]></value><valuecode><![CDATA['');  if PRFORMID#SET(1).visible then
htp.prn(''<span]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>79</elementid><pelementid>31</pelementid><orderby>-16</orderby><element>TR_</element><type>B</type><id></id><nameid>P_BLOCK-16</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><';
 v_vc(104) := 'source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>80</elementid><pelementid>32</pelementid><orderby>-18</orderby><element>TX_</element><type>E</type><id></id><nameid>PRFORMID_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxPRFORMID rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxPRFORMID rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxPRFORMID_NAME]]></value><valuecode><![CDATA[="rasdTxPRFORMID_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute';
 v_vc(105) := '></attributes></element><element><elementid>81</elementid><pelementid>80</pelementid><orderby>1</orderby><element>SELECT_</element><type>D</type><id>PRFORMID</id><nameid>PRFORMID</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdSelect <% if PRFORMID#SET(1).error is not null then htp.p('' errorField''); end if; %><% if PRFORMID#SET(1).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(PRFORMID#SET(1).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(PRFORMID#SET(1).custom , instr(upper(PRFORMID#SET(1).custom),''CLASS="'')+7 , instr(upper(PRFORMID#SET(1).custom),''"'',instr(upper(PRFORMID#SET(1).custom),''CLASS="'')+8)-instr(upper(PRFORMID#SET(1).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdSelect '');  if PRFORMID#SET(1).error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if PRFORMID#SET(1).info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(PRFORMID#SET(1).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(PRFORMID#SET(1).custom , instr(upper(PRFORMID#SET(1).custom),''CLASS="'')+7 , instr(upper(PRFORMID#SET(1).custom),''"'',instr(upper(PRFORMID#SET(1).custom),''CLASS="'')+8)-instr(upper(PRFORMID#SET(1).custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[ID]]></name><value><![CDATA[PRFORMID_NAME_RASD]]></value><valuecode><![CDATA[="PRFORMID_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><';
 v_vc(106) := 'rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PRFORMID_NAME]]></value><valuecode><![CDATA[="PRFORMID_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PRFORMID#SET(1).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  ';
 v_vc(107) := 'if PRFORMID#SET(1).readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PRFORMID#SET(1).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if PRFORMID#SET(1).disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PRFORMID#SET(1).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if PRFORMID#SET(1).required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||PRFORMID#SET(1).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||PRFORMID#SET(1).custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PRFORMID#SET(1).error is not null then htp.p('' title="''||PRFORMID#SET(1).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if PRFORMID#SET(1).error is not null then htp.p('' title="''||PRFORMID#SET(1).error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text><';
 v_vc(108) := '/text><name></name><value><![CDATA[<% if PRFORMID#SET(1).info is not null then htp.p('' title="''||PRFORMID#SET(1).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if PRFORMID#SET(1).info is not null then htp.p('' title="''||PRFORMID#SET(1).info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</select><% end if; %>]]></value><valuecode><![CDATA[</select>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if PRFORMID#SET(1).visible then %><select]]></value><valuecode><![CDATA['');  if PRFORMID#SET(1).visible then
htp.prn(''<select]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[lov$RfForms]]></value><valuecode><![CDATA[''); js_Slov$RfForms(PRFORMID(1),''PRFORMID_1'');
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>82</elementid><pelementid>37</pelementid><orderby>20</orderby><element>TX_</element><type>E</type><id></id><nameid>B10FORM_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10FORM rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10FORM rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></va';
 v_vc(109) := 'lueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10FORM_NAME]]></value><valuecode><![CDATA[="rasdTxB10FORM_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>83</elementid><pelementid>82</pelementid><orderby>1</orderby><element>FONT_</element><type>D</type><id>B10FORM</id><nameid>B10FORM</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDA';
 v_vc(110) := 'TA[id]]></name><value><![CDATA[B10FORM_NAME_RASD]]></value><valuecode><![CDATA[="B10FORM_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid>';
 v_vc(111) := '</attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[B10FORM_VALUE]]></value><valuecode><![CDATA[''||B10FORM(iB10)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>84</elementid><pelementid>37</pelementid><orderby>40</orderby><element>TX_</element><type>E</type><id></id><nameid>B10LABEL_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10LABEL rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10LABEL rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10LABEL_NAME]]></value><valuecode><![CDATA[="rasdTxB10LABEL_''||iB10||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name>';
 v_vc(112) := '<value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>85</elementid><pelementid>84</pelementid><orderby>1</orderby><element>FONT_</element><type>D</type><id>B10LABEL</id><nameid>B10LABEL</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10LABEL_NAME_RASD]]></value><valuecode><![CDATA[="B10LABEL_''||iB10||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><';
 v_vc(113) := 'rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[B10LABEL_VALUE]]></value><valuecode><![CDATA[''||B10LABEL(iB10)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>86</elementid><pelementid>48</pelementid><orderby>60</orderby><element>TX_</element><type>E</type><id></id><nameid>B20FORM_TX</nameid><endtagelementid>0</endtagelementid><sourc';
 v_vc(114) := 'e></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB20FORM rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB20FORM rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB20FORM_NAME]]></value><valuecode><![CDATA[="rasdTxB20FORM_''||iB20||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>87</elementid><pelementid>86</pelementid><orderby>1</orderby><element>FONT_</element><type>D</type><id>B20FORM</id><nameid>B20FORM</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attr';
 v_vc(115) := 'ibute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20FORM_NAME_RASD]]></value><valuecode><![CDATA[="B20FORM_''||iB20||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></text';
 v_vc(116) := 'id><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[B20FORM_VALUE]]></value><valuecode><![CDATA[''||B20FORM(iB20)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>88</elementid><pelementid>48</pelementid><orderby>80</orderby><element>TX_</element><type>E</type><id></id><nameid>B20LABEL_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB20LABEL rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB20LABEL rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB20LABEL_NAME]]></value><valuecode><![CDATA[="rasdTxB20LABEL_''||iB20||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute';
 v_vc(117) := '>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>89</elementid><pelementid>88</pelementid><orderby>1</orderby><element>FONT_</element><type>D</type><id>B20LABEL</id><nameid>B20LABEL</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20LABEL_NAME_RASD]]></value><valuecode><![CDATA[="B20LABEL_''||iB20||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><val';
 v_vc(118) := 'ueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[B20LABEL_VALUE]]></value><valuecode><![CDATA[''||B20LA';
 v_vc(119) := 'BEL(iB20)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element></elements></form>';
     return v_vc;
  end;
function metadata return clob is
  v_clob clob := '';
  v_vc cctab;
  begin
     v_vc := metadata_xml;
     for i in 1..v_vc.count loop
       v_clob := v_clob || v_vc(i);
     end loop;
     return v_clob;
  end;
procedure metadata is
  v_clob clob := '';
  v_vc cctab;
  begin
  owa_util.mime_header('text/xml', FALSE);
  HTP.p('Content-Disposition: filename="Export_RASDC2_REFERENCES_v.1.1.20240228091044.xml"');
  owa_util.http_header_close;
  htp.p('<?xml version="1.0" encoding="UTF-8" ?>');
     v_vc := metadata_xml;
     for i in 1..v_vc.count loop
       htp.prn(v_vc(i));
     end loop;
  end;
     begin
       null;
  -- initialization part

end RASDC2_REFERENCES;

/
